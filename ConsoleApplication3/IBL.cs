﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using BL_backend;

namespace BL
{
    public interface IBL
    {

        void addUser(User U);

        void removeUser(User U);

        bool FindProduct(int inventoryId);

        bool FindEmployeeByID(int teudatZeut);

        bool FindClubMemberByID(int teudatZeut);

        bool FindTransactionByID(string transactionID);

        bool FindDepartmentByID(int departemntID);

        bool FindUser(string userName, int password);

        void updateUser(string userName, int pass, string NuserName, User.userType type);

        void updateClubMember(int teudatZeut, int newTeu, string fName, string Lname, DateTime dateOfBirth, string gender, int memberID, DateTime cardExp, long cardNum, int cardThreeDig);

        void updateEmployee(int teudatZeut, string fName, double salary, string Lname, string gender, int newTeu, int departmentID, int supervisor);

        void updateDepartment(int departmentId, string name, int newDepId);

        void updateTransaction(string transactionID, bool isReturn, DateTime dateTime, PaymentMethod paymentMethod, string newTranId);

        void AddNewTransactionId(int ClubSSN, string tranId);

        void AddnewProduct(string name, productTypes Type, int inveID, int location, bool inStock, int stockCount, double price);

        void deleteProduct(int inventoryId);

        void updateProduct(string name,int inventoryId, int stockCount, productTypes type, int location, double price, int newInv, bool inStock);

        void AddNewEmployee(int salary, int departmentID, int supervisor, int teudatZeut, string firstName, string lastName, string gender);

        void deleteEmployee(int teudatZeut);

        void AddnewClubMember(int memberID, DateTime dateOfBirth, int teudatZeut, string firstName, string lastName, string gender, User user, DateTime creditCardExpiration, long creditCardNumber, int creditCardThreeDigits);

        void deleteClubMember(int teudatZeut);

        // String GetEmpGender(int teudatZeut);

        Transaction AddnewTransaction(DateTime dateTime, bool isAReturn, PaymentMethod paymentMethod, Dictionary<int, BL_backend.Transaction.SoldProducts> receipt);

        void deleteTransaction(string transactionID);

        Product GetBestSeller(int month);

        List<Employee> FindManagerByID(int TZ);

        Employee getEmp(string userName);

        void AddNewDepartment(int departmentID, string name);

        void deleteDepartment(int departmentID);

        List<Employee> GetAllEmployeesBySalaryRange(double salary1, double salary2);

        List<ClubMember> GetClubMemberListByDateOfBirth(DateTime Age1, DateTime Age2);

        List<Transaction> GetAllTransactionListByDate(DateTime date,DateTime date2);

        List<Transaction> GetTransactionListByPaymentMethod(PaymentMethod PaymentMethod);

        List<Transaction> GetAllTransactionListByMonth(int month);

        List<ClubMember> GetAllClubMemberByClubID(int MemberID);

        List<ClubMember> GetAllClubMemberBySSN(int tz);

        List<Employee> GetAllEmployeeListByDepID(int dpID);

        List<Product> GetProductListByType(productTypes type);

        List<User> FindUserByType(BL_backend.User.userType type);

        List<Product> GetProductListByPrice(double price1, double price2);

        List<Department> GetDepartmentByName(string name);

     

        List<Product> GetAllProductsList();

        List<ClubMember> GetAllClubMembers();

        List<Employee> GetAllEmployees();

        List<Transaction> GetAllTransactions();

        List<Department> GetAllDepartments();

        List<User> GetAllUsers();

        User.userType FindUserType(string user, int pass);

        Product GetProductByName(string name);

        Product GetProductByID(int id);

        ClubMember GetClubMemberByUser(User _user);

        bool IsCreditCard(PaymentMethod payment);
    }
}