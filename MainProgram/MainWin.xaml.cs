﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DAL;
using BL;
using GUI;
using SQL_DB;
using UnitTestProject1;
namespace MainProgram
{

    public partial class MainWin : Window
    {
        public MainWin()
        {
            this.Hide();
            IDAL itsDal = new SQL_DAL();
            IBL itsBl = new product_BL(itsDal);
            MainWindow main = new MainWindow(itsBl);

            //UnitTest1 t = new UnitTest1();
            //t.addDuplicateProduct();
        }
    }
}
