﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BL;
using DAL;
using BL_backend;
using System.Threading;
using System.Collections;
using System.ComponentModel;


namespace GUI
{
    /// <summary>
    /// Interaction logic for PurchaseWindow.xaml
    /// </summary>
    public partial class PurchaseWindow : Window
    {



        load l;
        int count;
        public BackgroundWorker worker;
       // System.Windows.Threading.DispatcherTimer dispatcherTimer;
        IBL itsBL;
        User U;

        public PurchaseWindow(IBL ibl, User user)
        {
            InitializeComponent();
            InitWindow(ibl, user);
            worker = new BackgroundWorker();

        }

        private void InitWindow(IBL ibl, User user)
        {
            this.itsBL = ibl;
            this.U = user;
            string[] productTypes = Enum.GetNames(typeof(productTypes));
            cmbType.ItemsSource = productTypes;
            string[] paymentMethod = Enum.GetNames(typeof(PaymentMethod));
            cmbPay.ItemsSource = paymentMethod;
            LoadBestSeller();
            List<Product> productList = itsBL.GetAllProductsList();
            LoadProducts(productList);
            comboSelection();
            InitControls(U.Type);

        }

        private void InitControls(BL_backend.User.userType userType)
        {
            btnAdminPurchase.IsEnabled = false;
            btnPay.IsEnabled = false;
            btnPayAndSave.IsEnabled = false;
            switch (userType)
            {
                case User.userType.Admin:
                    btnAdminPurchase.IsEnabled = true;
                    break;
                case User.userType.Manager:
                    break;
                case User.userType.Worker:
                    break;
                case User.userType.Customer:
                    btnPay.IsEnabled = true;
                    break;
                case User.userType.ClubMember:
                    btnPay.IsEnabled = true;
                    break;
                default:
                    break;
            }
        }

        private void LoadProducts(List<Product> productList)
        {
            ListProduct.Items.Clear();
            if (productList == null)
            {
                productList = itsBL.GetAllProductsList();
            }
            count = productList.Count;
            //l = new load(count);
            //l.Show();
            //dispatcherTimer = new System.Windows.Threading.DispatcherTimer();
            //dispatcherTimer.Tick += dispatcherTimer_Tick;
            //dispatcherTimer.Interval = new TimeSpan(0, 0, 1);
            //dispatcherTimer.Start();

            foreach (Product product in productList)
            {
                ProductListItem item = new ProductListItem(product);

                ListProduct.Items.Add(item);
               // prgBar.Value++;

               // lblProg.Content = ((prgBar.Value) / prgBar.Maximum) * 100 + "%";
            }


        }
        void startwork()
        {
            worker.RunWorkerAsync();
            worker.DoWork += worker_DoWork;
            worker.ProgressChanged += worker_ProgressChanged;
            worker.RunWorkerCompleted += worker_RunWorkerCompleted;
        }

        void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            prgBar.Value = 100;
            prgBar.Value = 0;
        }

        void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            prgBar.Focus();
            prgBar.Value = e.ProgressPercentage;
        }

        void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            int per;
            worker.WorkerReportsProgress = true;
            per = 100 /   itsBL.GetAllProductsList().Count;
            for (int i = 1; i <  itsBL.GetAllProductsList().Count + 1; i++)
            {
                worker.ReportProgress(i * per);
                Thread.Sleep(250);
            }
        }

        private void LoadBestSeller()
        {
            Product product = itsBL.GetBestSeller(4);
            if (product != null)
            {
                lblBestSeller.Content = product.getName();
            }
        }

        private void ListCart_Drop(object sender, DragEventArgs e)
        {

            // If the DataObject contains string data, extract it. 
            if (e.Data.GetDataPresent(typeof(ProductListItem)))
            {
                ProductListItem dataItem = (ProductListItem)e.Data.GetData(typeof(ProductListItem));
                ListCart.Items.Add(new ProductListItem(dataItem));

            }

        }

        private void ListProduct_MouseMove(object sender, MouseEventArgs e)
        {
            ProductListItem item = ListProduct.SelectedItem as ProductListItem;
            if (item != null && e.LeftButton == MouseButtonState.Pressed)
            {
                DragDrop.DoDragDrop(item,
                                     item,
                                     DragDropEffects.Copy);
            }
        }


        //private void dispatcherTimer_Tick(object sender, EventArgs e)
        //{
        //    //Product2 p = new Product2();

        //    if (l.pbStatus.Value == count)
        //    {
        //        PurchaseWindow P = new PurchaseWindow(itsBL, U);
        //        this.Hide();
        //        P.Show();
        //        // this.pageFrame.Content = p;
        //        l.Visibility = Visibility.Hidden;
        //        dispatcherTimer.Stop();
        //    }
        //}


        private void btnSearchName_Click(object sender, RoutedEventArgs e)
        {

            Product product = itsBL.GetProductByName(txtName.Text);
            ListProduct.Items.Clear();
            if (product != null)
            {
                ProductListItem item = new ProductListItem(product);

                ListProduct.Items.Add(item);
            }
            else if (String.IsNullOrEmpty(txtName.Text))
            {
                List<Product> productList = itsBL.GetAllProductsList();
                LoadProducts(productList);
            }
        }

        private void txtName_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            txtName.Text = "";
        }

        private void txtID_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            txtID.Text = "";
        }



        private void cmbType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            productTypes type = (productTypes)Enum.Parse(typeof(productTypes), (string)cmbType.SelectedItem);

            List<Product> productList = itsBL.GetProductListByType(type);
            LoadProducts(productList);
        }


        private void txtThreeDigits_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            txtThreeDigits.Text = "";

        }

        private void txtCardNumber_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            txtCardNumber.Text = "";

        }

        private void btnSearchByID_Click(object sender, RoutedEventArgs e)
        {
            if (String.IsNullOrEmpty(txtID.Text))
            {
                List<Product> productList = itsBL.GetAllProductsList();
                LoadProducts(productList);
            }
            else
            {
                int n;

                bool success = int.TryParse(txtID.Text, out n);
                if (success)
                {
                    Product product = itsBL.GetProductByID(n);

                    ListProduct.Items.Clear();
                    if (product != null)
                    {
                        ProductListItem item = new ProductListItem(product);

                        ListProduct.Items.Add(item);
                    }
                }
                else
                {
                    MessageBox.Show("Invalid input");
                }
            }
        }

        public void comboSelection()
        {
            if (cmbType.Text.Equals("cash") || cmbType.Text.Equals("check") || cmbType.Text.Equals("other"))
            {
                txtCardNumber.IsEnabled = false;
               
                txtThreeDigits.IsEnabled = false;
            }
        }


        private void btnPayAndSave_Click(object sender, RoutedEventArgs e)
        {
              if (U.Type.Equals("Customer"))
                  MessageBox.Show("Sorry, you can't save your details since you're not a club member yet.");

          //  ClubMember clubMember = itsBL.GetClubMemberByUser(U);
            string clubName = U.UserName;
          //  itsBL.FindClubMemberByID();

            if (U.Type.Equals("clubMember"))
            {

                DateTime dateExp =(DateTime)dp.SelectedDate;
               
                long number = long.Parse(txtCardNumber.Text);
                int ssn = int.Parse(txtSSN.Text);
                int threeDig = int.Parse(txtThreeDigits.Text);           
              
                PaymentMethod payment = (PaymentMethod)Enum.Parse(typeof(PaymentMethod), (string)cmbPay.SelectedItem);
                Dictionary<int, Transaction.SoldProducts> receipt = GetReciept();
                ClubMember C = itsBL.GetClubMemberByUser(U);
                C.CreditCardExpiration = dateExp;
                C.CreditCardNumber = number;
                C.CreditCardThreeDigits = threeDig;


                if (receipt.Count > 0)
                {
                    Transaction tran = itsBL.AddnewTransaction(DateTime.Now, false, payment, receipt);
                   
                    MessageBox.Show("Your card's information saved successfully and you paid successfully!");
                }
                else
                {
                    MessageBox.Show("Please choose products");
                }
            }
        }

        private Dictionary<int, Transaction.SoldProducts> GetReciept()
        {
            bool flag = false;
            Dictionary<int, Transaction.SoldProducts> reciept = new Dictionary<int, Transaction.SoldProducts>();
            foreach (ProductListItem item in ListCart.Items)
            {
                int quantity = GetQuantity(item);
                if (quantity > 0 && quantity <= item.StockCount)
                {
                    Transaction.SoldProducts soldProd = new Transaction.SoldProducts(item.Product, quantity);
                    reciept.Add(item.InventoryID, soldProd);
                    int stockCount = item.StockCount - quantity;
                    itsBL.updateProduct(item.Name,item.InventoryID, stockCount, item.Type, item.Location, item.Price, item.InventoryID,true);
                    flag = true;
                }
                else
                {
                    MessageBox.Show("Invalid Quantity");
                }

            }
            if (flag)
            {
                return reciept;
            }
            else return null;
        }

        private double GetTotalPrice()
        {
            double result = 0;
            foreach (ProductListItem item in ListCart.Items)
            {
                int quantity = GetQuantity(item);
                result += quantity * item.Price;
            }
            return result;
        }

        private int GetQuantity(ProductListItem ListCartItem)
        {
            var gridView = ListCart.View as GridView;
            ListBoxItem myListBoxItem = ListCart.ItemContainerGenerator.ContainerFromItem(ListCartItem) as ListBoxItem;

            //ContentControl does not directly host the elements provided by the expanded DataTemplate.  
            //It uses a ContentPresenter to do the  work. 
            //If we want to use the FindName method we will have to pass that ContentPresenter as the second argument, instead of the ContentControl. 
            ContentPresenter myContentPresenter = GetVisualChild<ContentPresenter>(myListBoxItem);

            var grid = gridView.Columns[2].CellTemplate.FindName("txtListCartQuantity", myContentPresenter);
            string text = ((System.Windows.Controls.TextBox)(grid)).Text;

            int quantity;
            bool success = int.TryParse(text, out quantity);
            if (success)
                return quantity;
            return -1;
        }

        private T GetVisualChild<T>(Visual parent) where T : Visual
        {
            T child = default(T);

            for (int index = 0; index < VisualTreeHelper.GetChildrenCount(parent); index++)
            {
                Visual visualChild = (Visual)VisualTreeHelper.GetChild(parent, index);
                child = visualChild as T;

                if (child == null)
                    child = GetVisualChild<T>(visualChild);//Find Recursively

                else
                    return child;
            }
            return child;
        }


        private void cmbPay_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            PaymentMethod payment = (PaymentMethod)Enum.Parse(typeof(PaymentMethod), (string)cmbPay.SelectedItem);
            if (itsBL.IsCreditCard(payment))
            {
                btnPayAndSave.IsEnabled = true;
            }
            else
            {
                btnPayAndSave.IsEnabled = false;
            }
        }

        private void btnViewSum_Click(object sender, RoutedEventArgs e)
        {
            if (ListCart.Items != null)
            {
                {
                    double price = GetTotalPrice();
                    MessageBox.Show("The Total Sum is " + price);
                }

            }
        }

        private void btnPay_Click(object sender, RoutedEventArgs e)
        {


            if (cmbPay.SelectedIndex != -1)
            {
                if (cmbPay.SelectedItem.Equals("Visa") || cmbPay.SelectedItem.Equals("MasterCard") || cmbPay.SelectedItem.Equals("AmericanExpress") || cmbPay.SelectedItem.Equals("Isracard"))
                {
                    if (true)
                    {
                        if (itsBL.FindUserType(U.UserName, U.Password).Equals(User.userType.ClubMember))
                        {
                            PaymentMethod payment = (PaymentMethod)Enum.Parse(typeof(PaymentMethod), (string)cmbPay.SelectedItem);
                            Dictionary<int, Transaction.SoldProducts> receipt = GetReciept();
                            if (receipt.Count > 0)
                            {
                                Transaction tran = itsBL.AddnewTransaction(DateTime.Now, false, payment, receipt);
                                ClubMember clubMember = itsBL.GetClubMemberByUser(U);
                                itsBL.AddNewTransactionId(clubMember.memberID, tran.TransactionID);
                                MessageBox.Show("Your card's information saved successfully and you paid successfully!");
                            }
                            else
                            {
                                MessageBox.Show("Please choose products");
                            }
                        }
                        else
                        {
                            PaymentMethod payment = (PaymentMethod)Enum.Parse(typeof(PaymentMethod), (string)cmbPay.SelectedItem);
                            Dictionary<int, Transaction.SoldProducts> receipt = GetReciept();
                            if (receipt.Count > 0)
                            {
                                Transaction tran = itsBL.AddnewTransaction(DateTime.Now, false, payment, receipt);
                                ClubMember clubMember = itsBL.GetClubMemberByUser(U);
                                MessageBox.Show("paid successfully!");
                            }
                            else if(receipt.Count==0)
                            {
                                MessageBox.Show("Please choose products");
                            }

                        }
                    }

                }
                else
                {
                    PaymentMethod payment = (PaymentMethod)Enum.Parse(typeof(PaymentMethod), (string)cmbPay.SelectedItem);
                    Dictionary<int, Transaction.SoldProducts> receipt = GetReciept();
                    if (receipt.Count > 0)
                    {
                        Transaction tran = itsBL.AddnewTransaction(DateTime.Now, false, payment, receipt);
                        ClubMember clubMember = itsBL.GetClubMemberByUser(U);
                        itsBL.AddNewTransactionId(clubMember.memberID, tran.TransactionID);
                        MessageBox.Show("paid successfully!");
                    }
                    else
                    {
                        MessageBox.Show("Please choose products");
                    }
                }
            }
            else
            {
                MessageBox.Show("Pleas choose payment method");
            }
        }

        private void btnAdminPurchase_Click(object sender, RoutedEventArgs e)
        {
            if (cmbPay.SelectedIndex != -1)
            {
                if (itsBL.FindUserType(U.UserName, U.Password).Equals(User.userType.Admin))
                {
                    if ((cmbPay.SelectedItem.Equals("Visa") || cmbPay.SelectedItem.Equals("MasterCard") || cmbPay.SelectedItem.Equals("AmericanExpress") || cmbPay.SelectedItem.Equals("Isracard")))
                    {
                        //yyyyyyyyyy//  
                     if (true)
                        {
                            PaymentMethod payment = (PaymentMethod)Enum.Parse(typeof(PaymentMethod), (string)cmbPay.SelectedItem);

                            Dictionary<int, Transaction.SoldProducts> receipt = GetReciept();
                            if (receipt.Count > 0)
                            {
                                Transaction tran = itsBL.AddnewTransaction(DateTime.Now, false, payment, receipt);
                                ClubMember clubMember = itsBL.GetClubMemberByUser(U);
                                MessageBox.Show("Admin you have added transaction successfully!");
                            }
                            else
                            {
                                MessageBox.Show("Please choose products");
                            }

                        }
                        else
                        {
                            MessageBox.Show("Please try again");

                        }
                    }
                    else
                    {

                        PaymentMethod payment = (PaymentMethod)Enum.Parse(typeof(PaymentMethod), (string)cmbPay.SelectedItem);
                        Dictionary<int, Transaction.SoldProducts> receipt = GetReciept();
                        itsBL.AddnewTransaction(DateTime.Now, false, payment, receipt);
                        if (receipt.Count > 0)
                        {
                            Transaction tran = itsBL.AddnewTransaction(DateTime.Now, false, payment, receipt);
                            ClubMember clubMember = itsBL.GetClubMemberByUser(U);
                            MessageBox.Show("Admin you have added transaction successfully!");
                        }
                        else
                        {
                            MessageBox.Show("Please choose products");
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("Pleas choose payment method");
            }
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {

            if (itsBL.FindUserType(U.UserName, U.Password).Equals(User.userType.Admin))
            {
                adminMain adminMain = new adminMain(itsBL, U);
                this.Hide();
                adminMain.Show();
            }
            if (itsBL.FindUserType(U.UserName, U.Password).Equals(User.userType.Manager))
            {
                ManagerMenu ManMain = new ManagerMenu(itsBL, U);
                this.Hide();
                ManMain.Show();
            }
            if (itsBL.FindUserType(U.UserName, U.Password).Equals(User.userType.Worker))
            {
                WorkerMenu WorkMain = new WorkerMenu(itsBL, U);
                this.Hide();
                WorkMain.Show();
            }
            if ((itsBL.FindUserType(U.UserName, U.Password).Equals(User.userType.Customer) || (itsBL.FindUserType(U.UserName, U.Password).Equals(User.userType.ClubMember))))
            {
                PurchaseWindow P = new PurchaseWindow(itsBL, U);
                this.Hide();
                P.Show();
            }
        }

       

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Stores p = new Stores(itsBL, U);
            p.Show();
            this.Close();
        }

       
    }

}


