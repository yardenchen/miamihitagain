﻿create database mileston3;


create table Users
(
userName varchar(50) NOT NULL PRIMARY KEY,
password_ Integer NOT NULL

);

create table userType
(
userName varchar(50) NOT NULL PRIMARY KEY,
kind varchar(25) NOT NULL,
constraint per FOREIGN KEY(userName) REFERENCES Users(userName)
);

create table Person
(
teudatZeut Integer NOT NULL PRIMARY KEY,
firstName varchar (30) NOT NULL,
lastName varchar (30),
gender varchar (10), 

);

create table Employee
(
 
 salary Decimal(10,10),
 departmentID Integer,
 supervisor Integer,
 teudatZeut Integer NOT NULL PRIMARY KEY,
 constraint per FOREIGN KEY(teudatZeut) REFERENCES Person(teudatZeut)

);

create table ClubMember
(
memberID Integer NOT NULL,
dateOfBirth	DATE,
creditCardExpiration DATE,
creditCardNumber BIGINT,
creditCardThreeDigits Integer,
 teudatZeut Integer,
 userName varchar (50) NOT NULL,
 constraint per FOREIGN KEY(teudatZeut) REFERENCES Person(teudatZeut),
 constraint mem PRIMARY KEY (memberID, userName), constraint us FOREIGN KEY (userName)  REFERENCES Users(userName),
);

create table Product
(
name varchar (50) NOT NULL,
inventoryID Integer NOT NULL,
location Integer,
inStock BIT,
stockCount Integer,
price DECIMAL,

constraint prim PRIMARY KEY (name, inventoryID)
);

create table productTypes
(
name varchar (50) NOT NULL PRIMARY KEY,
kind varchar(50),

constraint prime FOREIGN KEY(name) REFERENCES Product(name)

);

  		
create table Transactions
(
dateTime DATE NOT NULL ,
isAReturn BIT NOT NULL,
transactionID varchar(1000) NOT NULL PRIMARY KEY,


);

		
create table PaymentMethod
(
transactionID varchar(1000) NOT NULL PRIMARY KEY,
type varchar (20) NOT NULL,

constraint pri FOREIGN KEY(transactionID) REFERENCES Transactions(transactionID)

);

create table receipt
(
transactionID varchar(1000) NOT NULL ,
keyReceipt Integer NOT NULL  ,

constraint pri FOREIGN KEY(transactionID) REFERENCES Transactions(transactionID),
constraint KEYS PRIMARY KEY(transactionID, keyReceipt)
);

create table SoldProducts
(
transactionID varchar(1000) NOT NULL PRIMARY KEY,
ID Integer NOT NULL,
quantity Integer NOT NULL,
price Decimal (10,10),
keyReceipt Integer NOT NULL,

constraint pri FOREIGN KEY(transactionID) REFERENCES Transactions(transactionID),
constraint pro FOREIGN KEY(ID) REFERENCES Product(InventoryID),
constraint reci FOREIGN KEY(keyReceipt) REFERENCES receipt(keyReceipt)

);


create table Department
(
supervisor Integer NOT NULL,
departmentID Integer NOT NULL PRIMARY KEY,
name varchar(50) ,
--constraint prime FOREIGN KEY(supervisor) REFERENCES Product(supervisor)
);








