﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BL;
using BL_backend;


namespace GUI
{
    /// <summary>
    /// Interaction logic for WorkerEditPass.xaml
    /// </summary>
    public partial class WorkerEditPass : Window
    {
         private IBL itsBL;
        private User U;
        List<Employee> employee;
        List<Employee> e;
        int count;
        public WorkerEditPass(IBL IBL, User U)
        {
            employee = new List<Employee>();
           
            this.itsBL = IBL;
            this.U = U;
            InitializeComponent();

            e = itsBL.GetAllEmployees();
            for (int i = 0; i < e.Count; i++)
            {
                if (e[i].FirstName.Equals(U.UserName))
                {
                    employee.Add(e[i]);
                    break;
                }
            
            }

            gridy_employee.ItemsSource = employee;
            


       
        }

     

         private void btnGoBack_Click(object sender, RoutedEventArgs e)
         {
             WorkerMenu main = new WorkerMenu(itsBL, U);
             main.Show();
             this.Close();
         }

         private void gridy_employee_SelectionChanged(object sender, SelectionChangedEventArgs e)
         {
           
             
           
         }

        
    }
}

/*
for (int i = 0; i < count; i++)
             {
                 if( ((Employee)e).firstName.Equals(U.UserName))
                 {
                     
                 }
*/