﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using DAL;
using BL_backend;
using BL;
using GUI;

namespace SQL_DB
{
    public class SQL_DAL : IDAL 
    {
        EmartDBDataContext db;

        static void Main(string[] args)
        {
            DataBaseCreate data = new DataBaseCreate();
            SQL_DAL mySql = new SQL_DAL();
            IBL itsbl = new product_BL(mySql);


        }

        public SQL_DAL()
        {
            DataBaseCreate data = new DataBaseCreate();
            db = new EmartDBDataContext();
        }


        public void deleteProductFromList(Product P)
        {
            foreach (Product p in db.Products)
            {

                if (p.inventoryID == P.inventoryID)
                {
                    db.ProductTypes.DeleteOnSubmit(P.ProductType);
                    db.Products.DeleteOnSubmit(P);
               
                    
                    try
                    {
                        db.SubmitChanges();
                    }
                    catch
                    {
                      Errors err = new Errors();
                      err.expRowRemove();
                    }
                }               
            }
        }

        public void deleteEmployeeFromList(Employee E)
        {
            foreach (Employee e in db.Employees)
            {
                if (e.teudatZeut == E.teudatZeut)
                {
                    db.Employees.DeleteOnSubmit(E);
                    try
                    {
                        db.SubmitChanges();
                    }
                    catch
                    {
                        Errors err = new Errors();
                        err.expRowRemove();
                    }
                }
            }
        }

        public void deleteClubMemberFromList(ClubMember C)
        {
            foreach (userType t in db.userTypes)
            {
                if (t.userName == C.userName)
                {
                    db.userTypes.DeleteOnSubmit(t);
                }            
            }

            RemoveUserList(C.User);

            foreach (ClubMember c in db.ClubMembers)
            {
                if (c.teudatZeut == C.teudatZeut)
                {
                    db.ClubMembers.DeleteOnSubmit(c);
                    try
                    {
                        db.SubmitChanges();
                    }
                    catch
                    {
                        Errors err = new Errors();
                        err.expRowRemove();
                    }
                }
            }
        }

        public void deleteTransactionFromList(Transaction T)
        {
            foreach (Transaction t in db.Transactions)
            {
                if (t.transactionID == T.transactionID)
                {
                    db.Transactions.DeleteOnSubmit(T);
                    try
                    {
                        db.SubmitChanges();
                    }
                    catch
                    {
                        Errors err = new Errors();
                        err.expRowRemove();
                    }
                }
            }
        }

        public void deleteDepatmentFromList(Department D)
        {
            foreach (Employee emp in db.Employees)
            {
                if (emp.departmentID == D.departmentID)
                {
                    db.Employees.DeleteOnSubmit(emp);
                }
            }


            foreach (Department d in db.Departments)
            {
                if (d.departmentID == D.departmentID)
                {
                    db.Departments.DeleteOnSubmit(D);
                    try
                    {
                        db.SubmitChanges();
                    }
                    catch
                    {
                        Errors err = new Errors();
                        err.expRowRemove();
                    }
                }
            }
        }

        public List<Product> GetProductList()
        {
            List<Product> pr = new List<Product>();
            foreach (Product p in db.Products)
            {
                pr.Add(p);
                
            }
            return pr;                       
        }

        public void SetProductrList(List<Product> Product)
        {
            db.Products.InsertAllOnSubmit(Product);
            db.SubmitChanges();
        }

        public List<Employee> GetEmployeeList()
        {
            List<Employee> em = new List<Employee>();
            foreach (Employee e in db.Employees)
            {
                em.Add(e);
            }
            return em;
                
        }

        public void SetEmployeeList(List<Employee> Employee)
        {
            db.Employees.InsertAllOnSubmit(Employee);
            db.SubmitChanges();
        }

        public List<ClubMember> GetClubMemberList()
        {
            List<ClubMember> cm = new List<ClubMember>();
            foreach (ClubMember c in db.ClubMembers)
            {
                cm.Add(c);
            }
            return cm;
        }

        public void SetClubMemberList(List<ClubMember> ClubMember)
        {
            db.ClubMembers.InsertAllOnSubmit(ClubMember);
            db.SubmitChanges();
        }

        public List<Transaction> GetTransactionList()
        {
            List<Transaction> tr = new List<Transaction>();
            foreach (Transaction t in db.Transactions)
            {
                tr.Add(t);
            }
            return tr;
        }

        public void SetTransactionList(List<Transaction> Transaction)
        {
            db.Transactions.InsertAllOnSubmit(Transaction);
            db.SubmitChanges();
        }

        public List<User> GetUserList()
        {
            List<User> ur = new List<User>();
            foreach (User u in db.Users)
            {
                ur.Add(u);
            }
            return ur;
        }

        public void SetDepartmentList(List<Department> Department)
        {
            db.Departments.InsertAllOnSubmit(Department);
            db.SubmitChanges();
        }

        public List<Department> GetDepartmentList()
        {
            List<Department> dp = new List<Department>();
            foreach (Department d in db.Departments)
            {
                dp.Add(d);
            }
            return dp;
        }



        public void SetUserList(List<User> User)
        {
            db.Users.InsertAllOnSubmit(User);
            db.SubmitChanges();
        }

        public void AddProduct(Product P)
        {
            //row from table
            Product toAdd = new Product();

            //first 
            toAdd.inStock = P.inStock;
            toAdd.inventoryID = P.inventoryID;
            toAdd.location = P.location;
            toAdd.name = P.name;
            toAdd.price = P.price;
            


            //second 
            ProductType row = new ProductType();
            row.type = Convert.ToString(P.ProductType);
            row.inventoryID = P.inventoryID;




            db.Products.InsertOnSubmit(toAdd);
            db.ProductTypes.InsertOnSubmit(row);

            try
            {
                db.SubmitChanges();
            }
            catch
            {

                db.Products.DeleteOnSubmit(toAdd);
                db.ProductTypes.DeleteOnSubmit(row);
                Errors err = new Errors();
                err.expRowAdd();

            }
        }

        public void AddEmployee(Employee E)
        {
            //row from table
            Employee toAdd = new Employee();

            //first 
            toAdd.departmentID = E.departmentID;
            toAdd.salary = E.salary;
            toAdd.supervisor = E.supervisor;
            toAdd.teudatZeut = E.teudatZeut;
            toAdd.Person.firstName = E.Person.firstName;
            toAdd.Person.lastName = E.Person.lastName;
            toAdd.Person.gender = E.Person.gender;

            db.Employees.InsertOnSubmit(toAdd);
           

            try
            {
                db.SubmitChanges();
            }
            catch
            {               
                Errors err = new Errors();
                err.expRowAdd();

            }
        }

        public void AddClubMember(ClubMember C)
        {
            //row from table
            ClubMember toAdd = new ClubMember();

            //first 
            toAdd.memberID = C.memberID;
            toAdd.creditCardExpiration = C.creditCardExpiration;
            toAdd.creditCardNumber = C.creditCardNumber;
            toAdd.creditCardThreeDigits = C.creditCardThreeDigits;
            toAdd.dateOfBirth = C.dateOfBirth;
            toAdd.teudatZeut = C.teudatZeut;
            toAdd.Person.firstName = C.Person.firstName;
            toAdd.Person.lastName = C.Person.lastName;
            toAdd.Person.gender = C.Person.gender;

            db.ClubMembers.InsertOnSubmit(toAdd);


            try
            {
                db.SubmitChanges();
            }
            catch
            {

                db.ClubMembers.DeleteOnSubmit(toAdd);

                Errors err = new Errors();
                err.expRowAdd();

            }
        }

        public void AddTransaction (Transaction T)      
        {
            //row from table
            Transaction toAdd = new Transaction();

            //first 
            toAdd.dateTime = T.dateTime;
            toAdd.isAReturn = T.isAReturn;
            toAdd.transactionID = T.transactionID;

            //second 
            PaymentMethod row = new PaymentMethod();
            row.type = Convert.ToString(T.PaymentMethod);
            row.transactionID = T.transactionID;

            //third
            foreach (SoldProduct sol in T.SoldProducts)
            {
                db.SoldProducts.InsertOnSubmit(sol);
            }
            


    
           db.Transactions.InsertOnSubmit(toAdd);
           db.PaymentMethods.InsertOnSubmit(row);

            try
            {
                db.SubmitChanges();
            }
            catch
            {

             
                Errors err = new Errors();
                err.expRowAdd();

            }
        }

        public void AddUserList(User U)
        {

            //row from table
            User toAdd = new User();

            //first 
           toAdd.userName  = U.userName;
             toAdd.password_= U.password_;
       

            //second 
            userType row = new userType();
            row.kind = Convert.ToString(U.userType);
            row.userName = U.userName;



            db.Users.InsertOnSubmit(toAdd);
            db.userTypes.InsertOnSubmit(row);

            try
            {
                db.SubmitChanges();
            }
            catch {

                db.Users.DeleteOnSubmit(toAdd);
                db.userTypes.DeleteOnSubmit(row);
                Errors err = new Errors();
                err.expRowAdd();

            }
        }

        public void AddDepartment(Department D)
        {
            //row from table
            Department toAdd = new Department();

            //first 
            toAdd.departmentID= D.departmentID;
            toAdd.name = D.name;


            db.Departments.InsertOnSubmit(toAdd);
           
            try
            {
                db.SubmitChanges();
            }
            catch
            {

               // db.Departments.DeleteOnSubmit(toAdd);
               
                Errors err = new Errors();
                err.expRowAdd();

            }
        }

        public void RemoveUserList(User U)
        {
            foreach (User user in db.Users)
            {
                if (U.userName == user.userName)
                {
                    db.userTypes.DeleteOnSubmit(user.userType);
                    db.Users.DeleteOnSubmit(user);
                  
                    try
                    {
                        db.SubmitChanges();
                    }
                    catch
                    {

                        Errors err = new Errors();
                        err.expRowRemove();

                    }
                    
                }

            }
          foreach (ClubMember C in db.ClubMembers)
          {
              if (C.User.userName == U.userName)
              {
                  db.ClubMembers.DeleteOnSubmit(C);
                  try
                  {
                      db.SubmitChanges();
                  }
                  catch
                  {

                      Errors err = new Errors();
                      err.expRowRemove();

                  }
              }
          }
        }

        public void updateUser(string userName, int pass, string NuserName, BL_backend.User.userType type)
        {
            foreach (User U in db.Users)
            {
                if (U.userName == userName)
                {

                    //first 
                    U.userName = NuserName;
                    U.password_ = pass;

                }
                   
            } 

             foreach (userType Ty in db.userTypes)
             {
                 if(Ty.userName==userName)
                 {
                    Ty.kind = type.ToString();
                    Ty.userName = NuserName;
                 }
             }
                  
            
            try
                    {
                        db.SubmitChanges();
                    }
                    catch
                    {

                        Errors err = new Errors();
                        err.expRowRemove();

                    }
                }
            
        

        public void updateProduct(string name, int inventoryId, int stockCount, productTypes type, int location, double price, int newInv, bool inStock)
        {

            foreach (Product p in db.Products)
            {
                if (p.inventoryID == inventoryId)
                {
                    p.inStock = inStock;
                    p.inventoryID = newInv;
                    p.location = location;
                    p.name = name;
                    p.price = price;
                    p.stockCount = stockCount;
                    
               
                }
            }
           
       
            //second 
            foreach (ProductType row in db.ProductTypes)
            {
                if (row.inventoryID == inventoryId)
                {
                    row.inventoryID = newInv;
                    row.type = type.ToString();
                }
            }
           

            try
            {
                db.SubmitChanges();
            }
            catch
            {

             
                Errors err = new Errors();
                err.expRowAdd();

            }
        }

        public void updateClubMemner(int teudatZeut, int newTeu, string fName, string Lname, DateTime dateOfBirth, string gender, int memberID, DateTime creditCardExpiration, long creditCardNumber, int creditCardThreeDigits)
        {
            foreach (ClubMember c in db.ClubMembers)
            {
                if (c.teudatZeut==teudatZeut)
                {
                    c.teudatZeut = newTeu;
                    c.memberID = memberID;
                    c.dateOfBirth = dateOfBirth;
                    c.creditCardNumber = creditCardNumber;
                    c.creditCardExpiration = creditCardExpiration;
                    c.creditCardThreeDigits = creditCardThreeDigits;
                    c.Person.gender = gender;
                    c.Person.firstName = fName;
                    c.Person.lastName = Lname;
                }
            }

            try
            {
                db.SubmitChanges();
            }
            catch
            {

              
                Errors err = new Errors();
                err.expRowAdd();

            }
        }

        public void updateEmployee(int teudatZeut, string fName, double salary, string Lname, string gender, int newTeu, int departmentID, int supervisor)
        {        
            foreach (Employee c in db.Employees)
            {
                if (c.teudatZeut==teudatZeut)
                {
                    c.teudatZeut = newTeu;
                    c.salary = salary;
                    c.supervisor = supervisor;
                    c.departmentID = departmentID;
                    c.Person.gender = gender;
                    c.Person.firstName = fName;
                    c.Person.lastName = Lname;
                }
            }

            foreach (Department d in db.Departments)
            {
                if (d.departmentID == departmentID)
                {
                    d.departmentID = departmentID;
                }
            }

            try
            {
                db.SubmitChanges();
            }
            catch
            {

              
                Errors err = new Errors();
                err.expRowAdd();

            }
        
        }

        public void updateDepartment(int departmentId, string name, int newDepId)
        {
            foreach (Department c in db.Departments)
            {
                if (c.departmentID == departmentId)
                {
                    c.departmentID = newDepId;
                    c.name = name;
                 
                }
            }

            foreach (Employee e in db.Employees)
            {
                if (e.departmentID == departmentId)
                {
                    e.departmentID = newDepId;
                }
            }

            try
            {
                db.SubmitChanges();
            }
            catch
            {


                Errors err = new Errors();
                err.expRowAdd();

            }
        }

        public void updateTransaction(string transactionID, bool isReturn, DateTime dateTime, BL_backend.PaymentMethod paymentMethod, string newTranId)
        {
            foreach (Transaction c in db.Transactions)
            {
                if (c.transactionID == transactionID)
                {
                    c.transactionID = newTranId;
                    c.isAReturn = isReturn;
                    c.dateTime = dateTime;       
                }
            }

            foreach (PaymentMethod p in db.PaymentMethods)
            {
                if (p.transactionID == transactionID)
                {
                    p.transactionID = newTranId;
                    p.type = paymentMethod.ToString();
                }
            }

            foreach (SoldProduct s in db.SoldProducts)
            {
                if (s.transactionID == transactionID)
                {
                    s.transactionID = newTranId;
                  
                }
            }

            try
            {
                db.SubmitChanges();
            }
            catch
            {


                Errors err = new Errors();
                err.expRowAdd();

            }
        }
        /// <summary>
        /// CHANGE@@@!!!
        /// </summary>
        /// <param name="memberId"></param>
        /// <param name="tranId"></param>
        public void AddNewTransactionId(int memberId, string tranId)
        {
            
                    TransactionIdList toAdd = new TransactionIdList(tranId, memberId);
                    db.TransactionIdLists.InsertOnSubmit(toAdd);
          
                   
             
            try
            {
                db.SubmitChanges();
            }
            catch
            {
       
                Errors err = new Errors();
                err.expRowAdd();

            }
        }

        public BL_backend.Product FindProductByID(int inventoryID)
        {
            BL_backend.Product P = null;
         foreach (Product p in db.Products)
         {
             if (p.inventoryID == inventoryID)
             {
                 P = new BL_backend.Product();
                 P.StockCount = p.stockCount.Value;
                 P.InventoryID1 = p.inventoryID;
                 P.InStock1 = p.inStock.Value;
                 P.Location = p.location.Value;
                 P.Name = p.name;
                 P.Price1 = p.price.Value;
                
             }
         }
         return P;
        }

        public BL_backend.Employee FindEmployeeByID(int teudatZeut)
        {
            BL_backend.Employee E = new BL_backend.Employee();
            foreach (Employee e in db.Employees)
            {
                if (e.teudatZeut == teudatZeut)
                {
                    E.firstName = e.Person.firstName;
                    E.lastName = e.Person.lastName;
                    E.Salary = e.salary.Value;
                    E.TeudatZeut = e.teudatZeut;
                    E.Supervisor = e.supervisor.Value;
                    E.Gender = e.Person.gender;
                }
            }
            return E;
        }

        public BL_backend.ClubMember FindClubMemberByID(int teudatZeut)
        {
            BL_backend.ClubMember C = new BL_backend.ClubMember();
            foreach (ClubMember e in db.ClubMembers)
            {
                if (e.teudatZeut == teudatZeut)
                {
                    C.firstName = e.Person.firstName;
                    C.lastName = e.Person.lastName;
                    C.dateOfBirth = e.dateOfBirth.Value;
                    C.Gender = e.Person.gender;
                    C.teudatZeut = e.teudatZeut.Value;
                    C.memberID = e.memberID;
                    C.CreditCardExpiration = e.creditCardExpiration.Value;
                    C.CreditCardNumber = e.creditCardNumber.Value;
                    C.CreditCardThreeDigits = e.creditCardThreeDigits.Value;
                    C.user = new BL_backend.User(e.userName,e.User.password_,BL_backend.User.userType.ClubMember);
                    
                    
                  
                }
            }
            return C;
        }

        public List<BL_backend.Employee> FindManagerByID(int TZ)
        {
            List<BL_backend.Employee> emp = new List<BL_backend.Employee>();
            foreach (Employee e in db.Employees)
            {
                if (e.teudatZeut == TZ)
                {
                    BL_backend.Employee toAdd = new BL_backend.Employee(e.salary.Value,e.departmentID,e.supervisor.Value,e.teudatZeut,e.Person.firstName,e.Person.lastName,e.Person.gender);
                    emp.Add(toAdd);
                }
            }
            return emp;
        }

        public BL_backend.Employee getEmp(string userName)
        {
            BL_backend.Employee emp =null ; 
            foreach (Employee e in db.Employees)
            {
                if (e.Person.firstName == userName)
                {
                    emp = new BL_backend.Employee(e.salary.Value, e.departmentID, e.supervisor.Value, e.teudatZeut, e.Person.firstName, e.Person.lastName, e.Person.gender);
                    

                }
            }
            return emp;
        }

        public BL_backend.Transaction FindTransactionByID(string transactionID)
        {
            BL_backend.Transaction tran = new BL_backend.Transaction(); ;
            Dictionary<int, BL_backend.Transaction.SoldProducts> soldPro2 = new Dictionary<int, BL_backend.Transaction.SoldProducts>();
            BL_backend.PaymentMethod method = new BL_backend.PaymentMethod() ;
            List<BL_backend.Transaction.SoldProducts> solp = new List<BL_backend.Transaction.SoldProducts>();
            foreach (Transaction t in db.Transactions)
            {
                if (t.transactionID == transactionID)
                {
                   string pay = t.PaymentMethod.ToString();
                    switch (pay)
                    {
                        case "Cash": method = BL_backend.PaymentMethod.Cash;
                                     break;
                        case "Visa": method = BL_backend.PaymentMethod.Visa;
                                     break;
                        case "Check": method = BL_backend.PaymentMethod.Check;
                                      break;
                        case "Isracard": method = BL_backend.PaymentMethod.Isracard;
                                         break;
                        case "MasterCard": method = BL_backend.PaymentMethod.MasterCard;
                                           break;
                        case "AmericanExpress": method = BL_backend.PaymentMethod.AmericanExpress;
                                                break;
                        case "Other": method = BL_backend.PaymentMethod.Other;
                                                break;
                         
                    }
                    tran = new BL_backend.Transaction(t.dateTime, t.isAReturn, method, soldPro2);
                }
            }

            foreach (SoldProduct sol in db.SoldProducts)
            {
                if (sol.transactionID == transactionID)
                { 
                 BL_backend.productTypes type = new productTypes();
                 string proType = sol.Product.ProductType.ToString();
                 switch (proType)
                 {
                        case "Cleaning": type = BL_backend.productTypes.Cleaning;
                                                break;
                        case "Electronics": type = BL_backend.productTypes.Electronics;
                                            break;
                        case "Furniture": type = BL_backend.productTypes.Furniture;
                                          break;
                        case "None": type = BL_backend.productTypes.None;
                                            break;
                 }
                 BL_backend.Product pro = new BL_backend.Product(sol.Product.name,type,sol.Product.inventoryID,sol.Product.location.Value,sol.Product.inStock.Value,sol.Product.stockCount.Value,sol.Product.price.Value);
                 BL_backend.Transaction.SoldProducts toAdd = new BL_backend.Transaction.SoldProducts(pro, sol.quantity);
                 solp.Add(toAdd);
                 }
            
            }
            for (int i = 0; i < solp.Count(); i++)
            {
                soldPro2.Add(i, solp[i]);
            }
            tran.Receipt = soldPro2;
                
            return tran;
        }

        public BL_backend.Department FindDepartmentById(int departmentId)
        {
            BL_backend.Department dp = null;
            foreach (Department dpDa in db.Departments)
            {
                if (dpDa.departmentID == departmentId)
                {
                    dp = new BL_backend.Department(dpDa.name, dpDa.departmentID);


                }
            }
            return dp;
        }

        public List<BL_backend.Product> GetProductListByType(productTypes type)
        {
            List<BL_backend.Product> proList = new List<BL_backend.Product>();
            foreach (Product proDa in db.Products)
            {
                if (proDa.ProductType.type == type.ToString())
                {
                    BL_backend.Product toAdd = new BL_backend.Product(proDa.name,type,proDa.inventoryID,proDa.location.Value,proDa.inStock.Value,proDa.stockCount.Value,proDa.price.Value);
                    proList.Add(toAdd);


                }
            }
            return proList;
        }
        

        public List<BL_backend.User> FindUserByType(BL_backend.User.userType type)
        {
            List<BL_backend.User> userList = new List<BL_backend.User>();
            foreach (User user in db.Users)
            {
                if (user.userType.kind == type.ToString())
                {
                    BL_backend.User toAdd = new BL_backend.User(user.userName,user.password_,type);
                    userList.Add(toAdd);


                }
            }
            return userList;
        }


        public BL_backend.User.userType FindUserType(string user, int pass)
        {
            BL_backend.User.userType type = new BL_backend.User.userType();
            foreach (userType userT in db.userTypes)
            {
                if (userT.userName == user)
                {

                    string strType = userT.kind.ToString();
                    switch (strType)
                    {
                        case "Admin": type = BL_backend.User.userType.Admin;
                            break;
                        case "ClubMember": type = BL_backend.User.userType.ClubMember;
                            break;
                        case "Customer": type = BL_backend.User.userType.Customer;
                            break;
                        case "Manager": type = BL_backend.User.userType.Manager;
                            break;
                        case "Worker": type = BL_backend.User.userType.Worker;
                            break;
                    }
                }
            }
            return type;
        }

        public List<BL_backend.Transaction> GetTransactiontListByMonth(int month)
        {
            List<BL_backend.Transaction> tranList = new List<BL_backend.Transaction>();
            List<BL_backend.Transaction.SoldProducts> solp = new List<BL_backend.Transaction.SoldProducts>();
            Dictionary<int, BL_backend.Transaction.SoldProducts> soldPro2 = new Dictionary<int, BL_backend.Transaction.SoldProducts>();
            BL_backend.Transaction toAdd = new BL_backend.Transaction();
            foreach (Transaction t in db.Transactions)      //find transation
            {
                if (t.dateTime.Month == month)
                {
           
            
                  BL_backend.PaymentMethod method = new BL_backend.PaymentMethod() ;
          
                   string pay = t.PaymentMethod.ToString();
                     switch (pay)
                    {
                        case "Cash": method = BL_backend.PaymentMethod.Cash;
                                     break;
                        case "Visa": method = BL_backend.PaymentMethod.Visa;
                                     break;
                        case "Check": method = BL_backend.PaymentMethod.Check;
                                      break;
                        case "Isracard": method = BL_backend.PaymentMethod.Isracard;
                                         break;
                        case "MasterCard": method = BL_backend.PaymentMethod.MasterCard;
                                           break;
                        case "AmericanExpress": method = BL_backend.PaymentMethod.AmericanExpress;
                                                break;
                        case "Other": method = BL_backend.PaymentMethod.Other;
                                                break;
                         
                    }
                    toAdd = new BL_backend.Transaction(t.dateTime, t.isAReturn, method, soldPro2);
                    toAdd.TransactionID = t.transactionID;
                }


                foreach (SoldProduct sol in db.SoldProducts)
                {
                    if (sol.transactionID == toAdd.TransactionID)  //find sold product in the reciept
                    {


                        BL_backend.productTypes type = new productTypes();
                        string proType = sol.Product.ProductType.ToString();
                        switch (proType)
                        {
                            case "Cleaning": type = BL_backend.productTypes.Cleaning;
                                break;
                            case "Electronics": type = BL_backend.productTypes.Electronics;
                                break;
                            case "Furniture": type = BL_backend.productTypes.Furniture;
                                break;
                            case "None": type = BL_backend.productTypes.None;
                                break;
                        }
                        BL_backend.Product pro = new BL_backend.Product(sol.Product.name, type, sol.Product.inventoryID, sol.Product.location.Value, sol.Product.inStock.Value, sol.Product.stockCount.Value, sol.Product.price.Value);
                        BL_backend.Transaction.SoldProducts soltoAdd = new BL_backend.Transaction.SoldProducts(pro, sol.quantity);
                        solp.Add(soltoAdd);        //add sold product to the list 
                    }
                }
            
            }
            for (int i = 0; i < solp.Count(); i++)     //adding sold product to the sold product to reciept
            {
                soldPro2.Add(i, solp[i]);
            }
            toAdd.Receipt = soldPro2;
                
            tranList.Add(toAdd);
               
            
            return tranList;
        }

        public List<BL_backend.Product> GetProductListByPrice(double price1, double price2)
        {
            List<BL_backend.Product> proList = new List<BL_backend.Product>();
            foreach (Product p in db.Products)
            {
                if (p.price > price1 && p.price < price2)
                {
                    BL_backend.productTypes type = new productTypes();
                    string proType = p.ProductType.ToString();
                    switch (proType)
                    {
                        case "Cleaning": type = BL_backend.productTypes.Cleaning;
                            break;
                        case "Electronics": type = BL_backend.productTypes.Electronics;
                            break;
                        case "Furniture": type = BL_backend.productTypes.Furniture;
                            break;
                        case "None": type = BL_backend.productTypes.None;
                            break;
                    }

                    BL_backend.Product toAdd = new BL_backend.Product(p.name, type, p.inventoryID, p.location.Value, p.inStock.Value, p.stockCount.Value, p.price.Value);
                    proList.Add(toAdd);
                }
            
            }
            return proList;
        }

      

        public List<BL_backend.Employee> GetEmployeeListByDepID(int dpID)
        {
            List<BL_backend.Employee> empList = new List<BL_backend.Employee>();
            foreach (Employee emp in db.Employees)
            {
                if (emp.departmentID == dpID)
                {
                    BL_backend.Employee toAdd = new BL_backend.Employee(emp.salary.Value,emp.departmentID,emp.supervisor.Value,emp.teudatZeut,emp.Person.firstName,emp.Person.lastName,emp.Person.gender);
                    empList.Add(toAdd);

                }
            }
            return empList;
        }
        

        public List<BL_backend.Employee> GetEmployeeListBySalary(double Salary1, double Salary2)
        {
            List<BL_backend.Employee> empList = new List<BL_backend.Employee>();
            foreach (Employee emp in db.Employees)
            {
                if(emp.salary>Salary1 && emp.salary<Salary2)
                {
                    BL_backend.Employee toAdd = new BL_backend.Employee(emp.salary.Value,emp.departmentID,emp.supervisor.Value,emp.teudatZeut,emp.Person.firstName,emp.Person.lastName,emp.Person.gender);
                    empList.Add(toAdd);

                }
            }
            return empList;
        }

        public List<BL_backend.ClubMember> GetClubMemberByClubID(int MemberID)
        {
            List<BL_backend.ClubMember> clubList = new List<BL_backend.ClubMember>();
            foreach (ClubMember cm in db.ClubMembers)
            {
                if(cm.memberID==MemberID)
                {
                    string strType = cm.User.userType.kind.ToString();
                    BL_backend.User.userType type = new BL_backend.User.userType() ;
                    switch (strType)
                    {
                        case "Admin": type = BL_backend.User.userType.Admin;
                            break;
                        case "ClubMember": type = BL_backend.User.userType.ClubMember;
                            break;
                        case "Customer": type = BL_backend.User.userType.Customer;
                            break;
                        case "Manager": type = BL_backend.User.userType.Manager;
                            break;
                        case "Worker": type = BL_backend.User.userType.Worker;
                            break;
                    }


                    BL_backend.User user = new BL_backend.User(cm.userName , cm.User.password_ , type);
                    BL_backend.ClubMember toAdd = new BL_backend.ClubMember(cm.memberID, cm.dateOfBirth.Value, cm.Person.teudatZeut, cm.Person.firstName, cm.Person.lastName, cm.Person.gender, user, cm.creditCardExpiration.Value, cm.creditCardNumber.Value, cm.creditCardThreeDigits.Value);
                    List<string> tranList=new List<string>();
                    foreach(TransactionIdList tran in db.TransactionIdLists)
                    {
                        if (tran.memberID == cm.memberID)
                        {
                            tranList.Add(tran.transactionID);
                        
                        }
                    }
                    toAdd.transactionID = tranList;
                    clubList.Add(toAdd);
                }
            }
            return clubList;
        
        }

        public List<BL_backend.ClubMember> GetClubMemberListByDateOfBirth(DateTime DT1, DateTime DT2)
        {
            List<BL_backend.ClubMember> clubList = new List<BL_backend.ClubMember>();
            foreach (ClubMember cm in db.ClubMembers)
            {
                if(cm.dateOfBirth>DT1 && cm.dateOfBirth<DT2)
                {
                    string strType = cm.User.userType.kind.ToString();
                    BL_backend.User.userType type = new BL_backend.User.userType() ;
                    switch (strType)
                    {
                        case "Admin": type = BL_backend.User.userType.Admin;
                            break;
                        case "ClubMember": type = BL_backend.User.userType.ClubMember;
                            break;
                        case "Customer": type = BL_backend.User.userType.Customer;
                            break;
                        case "Manager": type = BL_backend.User.userType.Manager;
                            break;
                        case "Worker": type = BL_backend.User.userType.Worker;
                            break;
                    }


                    BL_backend.User user = new BL_backend.User(cm.userName , cm.User.password_ , type);
                    BL_backend.ClubMember toAdd = new BL_backend.ClubMember(cm.memberID, cm.dateOfBirth.Value, cm.Person.teudatZeut, cm.Person.firstName, cm.Person.lastName, cm.Person.gender, user, cm.creditCardExpiration.Value, cm.creditCardNumber.Value, cm.creditCardThreeDigits.Value);
                    List<string> tranList=new List<string>();
                    foreach(TransactionIdList tran in db.TransactionIdLists)
                    {
                        if (tran.memberID == cm.memberID)
                        {
                            tranList.Add(tran.transactionID);
                        
                        }
                    }
                    toAdd.transactionID = tranList;
                    clubList.Add(toAdd);
                }
            }
            return clubList;
        
        }
        public List<BL_backend.Transaction> GetTransactionListByPaymentMethod(BL_backend.PaymentMethod PaymentMethod)
        {

            List<BL_backend.Transaction> tranList = new List<BL_backend.Transaction>();
            //tranList.RemoveRange(0, tranList.Count);
            List<BL_backend.Transaction.SoldProducts> solp = new List<BL_backend.Transaction.SoldProducts>();
            
            BL_backend.Transaction toAdd = new BL_backend.Transaction();
            foreach (Transaction t in db.Transactions)      //find transation
            {
                if (t.PaymentMethod.type == PaymentMethod.ToString())
                {
                    Dictionary<int, BL_backend.Transaction.SoldProducts> soldPro2 = new Dictionary<int, BL_backend.Transaction.SoldProducts>();
                    toAdd = new BL_backend.Transaction(t.dateTime, t.isAReturn, PaymentMethod, soldPro2);
                    toAdd.TransactionID = t.transactionID;



                    foreach (SoldProduct sol in db.SoldProducts)
                    {
                        if (sol.transactionID == toAdd.TransactionID)  //find sold product in the reciept
                        {

                            BL_backend.productTypes type = new productTypes();
                            string proType = sol.Product.ProductType.ToString();
                            switch (proType)
                            {
                                case "Cleaning": type = BL_backend.productTypes.Cleaning;
                                    break;
                                case "Electronics": type = BL_backend.productTypes.Electronics;
                                    break;
                                case "Furniture": type = BL_backend.productTypes.Furniture;
                                    break;
                                case "None": type = BL_backend.productTypes.None;
                                    break;
                            }
                            BL_backend.Product pro = new BL_backend.Product(sol.Product.name, type, sol.Product.inventoryID, sol.Product.location.Value, sol.Product.inStock.Value, sol.Product.stockCount.Value, sol.Product.price.Value);
                            BL_backend.Transaction.SoldProducts soltoAdd = new BL_backend.Transaction.SoldProducts(pro, sol.quantity);
                            solp.Add(soltoAdd);        //add sold product to the list 
                        }

                    }
                    for (int i = 0; i < solp.Count(); i++)     //adding sold product to the sold product to reciept
                    {
                        soldPro2.Add(i, solp[i]);
                    }
                    toAdd.Receipt = soldPro2;
                    tranList.Add(toAdd);
                }

              
            }

            return tranList;
        }

        public List<BL_backend.Transaction> GetTransactiontListIsDate(DateTime date, DateTime date2)
        {

            List<BL_backend.Transaction> tranList = new List<BL_backend.Transaction>();
            List<BL_backend.Transaction.SoldProducts> solp = new List<BL_backend.Transaction.SoldProducts>();
            Dictionary<int, BL_backend.Transaction.SoldProducts> soldPro2 = new Dictionary<int, BL_backend.Transaction.SoldProducts>();
            BL_backend.Transaction toAdd = new BL_backend.Transaction();
            foreach (Transaction t in db.Transactions)      //find transation
            {
                if (t.dateTime >= date && t.dateTime <= date2)
                {


                    BL_backend.PaymentMethod method = new BL_backend.PaymentMethod();

                    string pay = t.PaymentMethod.ToString();
                    switch (pay)
                    {
                        case "Cash": method = BL_backend.PaymentMethod.Cash;
                            break;
                        case "Visa": method = BL_backend.PaymentMethod.Visa;
                            break;
                        case "Check": method = BL_backend.PaymentMethod.Check;
                            break;
                        case "Isracard": method = BL_backend.PaymentMethod.Isracard;
                            break;
                        case "MasterCard": method = BL_backend.PaymentMethod.MasterCard;
                            break;
                        case "AmericanExpress": method = BL_backend.PaymentMethod.AmericanExpress;
                            break;
                        case "Other": method = BL_backend.PaymentMethod.Other;
                            break;

                    }
                    toAdd = new BL_backend.Transaction(t.dateTime, t.isAReturn, method, soldPro2);
                }


                foreach (SoldProduct sol in db.SoldProducts)
                {
                    if (sol.transactionID == toAdd.TransactionID)  //find sold product in the reciept
                    {


                        BL_backend.productTypes type = new productTypes();
                        string proType = sol.Product.ProductType.ToString();
                        switch (proType)
                        {
                            case "Cleaning": type = BL_backend.productTypes.Cleaning;
                                break;
                            case "Electronics": type = BL_backend.productTypes.Electronics;
                                break;
                            case "Furniture": type = BL_backend.productTypes.Furniture;
                                break;
                            case "None": type = BL_backend.productTypes.None;
                                break;
                        }
                        BL_backend.Product pro = new BL_backend.Product(sol.Product.name, type, sol.Product.inventoryID, sol.Product.location.Value, sol.Product.inStock.Value, sol.Product.stockCount.Value, sol.Product.price.Value);
                        BL_backend.Transaction.SoldProducts soltoAdd = new BL_backend.Transaction.SoldProducts(pro, sol.quantity);
                        solp.Add(soltoAdd);        //add sold product to the list 
                    }

                }
                for (int i = 0; i < solp.Count(); i++)     //adding sold product to the sold product to reciept
                {
                    soldPro2.Add(i, solp[i]);
                }
                toAdd.Receipt = soldPro2;

                tranList.Add(toAdd);
            }

            return tranList;
        }

        public List<BL_backend.Department> GetDepartmentByName(string name)
        {
            List<BL_backend.Department> dp = new List<BL_backend.Department>();
            foreach (Department d in db.Departments)
            {
                if (d.name == name)
                {
                    BL_backend.Department toAdd = new BL_backend.Department(d.name, d.departmentID);
                    dp.Add(toAdd);
                }
            }
            return dp;
        }

       

        public bool IsUserExist(BL_backend.User user)
        {
         
            foreach (User userT in db.Users)
            {
                if (userT.userName == user.UserName && user.Password==userT.password_)
                {
                    return true;         
                }
            }
            return false;
        }

        public List<BL_backend.ClubMember> GetAllClubMemberBySSN(int tz)
        {

            List<BL_backend.ClubMember> clubList = new List<BL_backend.ClubMember>();
            foreach (ClubMember cm in db.ClubMembers)
            {
                if (cm.Person.teudatZeut == tz)
                {
                    string strType = cm.User.userType.kind.ToString();
                    BL_backend.User.userType type = new BL_backend.User.userType();
                    switch (strType)
                    {
                        case "Admin": type = BL_backend.User.userType.Admin;
                            break;
                        case "ClubMember": type = BL_backend.User.userType.ClubMember;
                            break;
                        case "Customer": type = BL_backend.User.userType.Customer;
                            break;
                        case "Manager": type = BL_backend.User.userType.Manager;
                            break;
                        case "Worker": type = BL_backend.User.userType.Worker;
                            break;
                    }


                    BL_backend.User user = new BL_backend.User(cm.userName, cm.User.password_, type);
                    BL_backend.ClubMember toAdd = new BL_backend.ClubMember(cm.memberID, cm.dateOfBirth.Value, cm.Person.teudatZeut, cm.Person.firstName, cm.Person.lastName, cm.Person.gender, user, cm.creditCardExpiration.Value, cm.creditCardNumber.Value, cm.creditCardThreeDigits.Value);
                    List<string> tranList = new List<string>();
                    foreach (TransactionIdList tran in db.TransactionIdLists)
                    {
                        if (tran.memberID == cm.memberID)
                        {
                            tranList.Add(tran.transactionID);

                        }
                    }
                    toAdd.transactionID = tranList;
                    clubList.Add(toAdd);
                }
            }
            return clubList;

        }

        public void deleteProductFromList(BL_backend.Product P)
        {
            foreach (ProductType ty in db.ProductTypes)
            {
                if (ty.inventoryID == P.InventoryID1)
                {
                    db.ProductTypes.DeleteOnSubmit(ty);
                }
            }

           
            foreach (Product p in db.Products)
            {
                if (p.inventoryID == P.InventoryID1)
                {
                    db.Products.DeleteOnSubmit(p);

                }
            }
            try
            {
                db.SubmitChanges();
            }
            catch
            {
                Errors err = new Errors();
                err.expRowRemove();
            }
        }

        public void deleteEmployeeFromList(BL_backend.Employee E)
        {
            foreach (Employee emp in db.Employees)
            {
                if (emp.teudatZeut == E.teudatZeut)
                {
                    db.Employees.DeleteOnSubmit(emp);

                }
            }

            foreach (Person p in db.Persons)
            {
                if (p.teudatZeut == E.teudatZeut)
                {
                    db.Persons.DeleteOnSubmit(p);
                }
            }

            try
            {
                db.SubmitChanges();
            }
            catch
            {
                Errors err = new Errors();
                err.expRowRemove();
            }
        }

        public void deleteClubMemberFromList(BL_backend.ClubMember C)
        {
            foreach (userType u in db.userTypes)
            {
                if (u.userName == C.user.UserName)
                    db.userTypes.DeleteOnSubmit(u);
            
            }

            foreach (User user in db.Users)
            {
                if (user.userName == C.user.UserName)
                {
                    db.Users.DeleteOnSubmit(user);
                }
            }


            foreach (ClubMember club in db.ClubMembers)
            {
                if (club.teudatZeut == C.teudatZeut)
                {
                    db.ClubMembers.DeleteOnSubmit(club);

                }
            }

            foreach (Person p in db.Persons)
            {
                if (p.teudatZeut == C.teudatZeut)
                {
                    db.Persons.DeleteOnSubmit(p);
                }
            }

            try
            {
                db.SubmitChanges();
            }
            catch
            {
                Errors err = new Errors();
                err.expRowRemove();
            }
        }

        public void deleteTransactionFromList(BL_backend.Transaction T)
        {
            foreach (TransactionIdList tranid in db.TransactionIdLists)
            {
                if (tranid.transactionID == T.TransactionID)
                {
                    db.TransactionIdLists.DeleteOnSubmit(tranid);
                }
            }

            foreach (SoldProduct sol in db.SoldProducts)
            {
                if (sol.transactionID == T.TransactionID)
                {
                    db.SoldProducts.DeleteOnSubmit(sol);
                }
            }


            foreach (Transaction tran in db.Transactions)
            {
                if (tran.transactionID == T.TransactionID)
                {
                    db.Transactions.DeleteOnSubmit(tran);

                }
            }
            try
            {
                db.SubmitChanges();
            }
            catch
            {
                Errors err = new Errors();
                err.expRowRemove();
            }
        }

        public void deleteDepatmentFromList(BL_backend.Department D)
        {
            foreach(Employee emp in db.Employees)
            {
                if (emp.departmentID == D.DepartmentID)
                {
                    db.Employees.DeleteOnSubmit(emp);
                }
            }

            foreach (Department dp in db.Departments)
            {
                if (dp.departmentID == D.DepartmentID)
                {
                    db.Departments.DeleteOnSubmit(dp);

                }
            }
            try
            {
                db.SubmitChanges();
            }
            catch
            {
                Errors err = new Errors();
                err.expRowRemove();
            }
        }

        public void SetProductrList(List<BL_backend.Product> Product)
        {
            foreach (Product p in db.Products)
            {
                    db.Products.DeleteOnSubmit(p);

                
            }
            try
            {
                db.SubmitChanges();
            }
            catch
            {
                Errors err = new Errors();
                err.expRowRemove();
            }

            foreach (BL_backend.Product p in Product)
            {
                AddProduct(p);  
            }
        }

        List<BL_backend.Employee> IDAL.GetEmployeeList()
        {
            List<BL_backend.Employee> empList = new List<BL_backend.Employee>();
            foreach (Employee emp in db.Employees)
            {
               
                
                 BL_backend.Employee toAdd = new BL_backend.Employee(emp.salary.Value, emp.departmentID, emp.supervisor.Value, emp.teudatZeut, emp.Person.firstName, emp.Person.lastName, emp.Person.gender);
                 empList.Add(toAdd);

                
            }
            return empList;
        }

        List<BL_backend.Product> IDAL.GetProductList()
        {
            List<BL_backend.Product> proList = new List<BL_backend.Product>();
            foreach (Product p in db.Products)
            {
               
                
                    BL_backend.productTypes type = new productTypes();
                    string proType = p.ProductType.type;
                    switch (proType)
                    {
                        case "Cleaning": type = BL_backend.productTypes.Cleaning;
                            break;
                        case "Electronics": type = BL_backend.productTypes.Electronics;
                            break;
                        case "Furniture": type = BL_backend.productTypes.Furniture;
                            break;
                        case "None": type = BL_backend.productTypes.None;
                            break;
                    }

                    BL_backend.Product toAdd = new BL_backend.Product(p.name, type, p.inventoryID, p.location.Value, p.inStock.Value, p.stockCount.Value, p.price.Value);
                    proList.Add(toAdd);
                

            }
            return proList;
        }

        public void SetEmployeeList(List<BL_backend.Employee> Employee)
        {
            foreach (Employee emp in db.Employees)
            {
                db.Employees.DeleteOnSubmit(emp);


            }
            try
            {
                db.SubmitChanges();
            }
            catch
            {
               Errors err = new Errors();
                err.expRowAdd();
            }

            foreach (BL_backend.Employee emp in Employee)
            {
                AddEmployee(emp);
            }
        }

        List<BL_backend.ClubMember> IDAL.GetClubMemberList()
        {

            List<BL_backend.ClubMember> clubList = new List<BL_backend.ClubMember>();
            foreach (ClubMember cm in db.ClubMembers)
            {
              
                
                    string strType = cm.User.userType.kind.ToString();
                    BL_backend.User.userType type = new BL_backend.User.userType();
                    switch (strType)
                    {
                        case "Admin": type = BL_backend.User.userType.Admin;
                            break;
                        case "ClubMember": type = BL_backend.User.userType.ClubMember;
                            break;
                        case "Customer": type = BL_backend.User.userType.Customer;
                            break;
                        case "Manager": type = BL_backend.User.userType.Manager;
                            break;
                        case "Worker": type = BL_backend.User.userType.Worker;
                            break;
                    }


                    BL_backend.User user = new BL_backend.User(cm.userName, cm.User.password_, type);
                    BL_backend.ClubMember toAdd = new BL_backend.ClubMember(cm.memberID, cm.dateOfBirth.Value, cm.Person.teudatZeut, cm.Person.firstName, cm.Person.lastName, cm.Person.gender, user, cm.creditCardExpiration.Value, cm.creditCardNumber.Value, cm.creditCardThreeDigits.Value);
                    List<string> tranList = new List<string>();
                    foreach (TransactionIdList tran in db.TransactionIdLists)
                    {
                        if (tran.memberID == cm.memberID)
                        {
                            tranList.Add(tran.transactionID);

                        }
                    }
                    toAdd.transactionID = tranList;
                    clubList.Add(toAdd);
                }
            
            return clubList;

        }

        public void SetClubMemberList(List<BL_backend.ClubMember> ClubMember)
        {
            foreach (SoldProduct sol in db.SoldProducts)
            {
                db.SoldProducts.InsertOnSubmit(sol);
            }
            
            
            foreach (ClubMember club in db.ClubMembers)
            {
                db.Users.DeleteOnSubmit(club.User);
                db.Persons.DeleteOnSubmit(club.Person);
                db.ClubMembers.DeleteOnSubmit(club);
            }
            try
            {
                db.SubmitChanges();
            }
            catch
            {
                Errors err = new Errors();
                err.expRowAdd();
            }

            foreach (BL_backend.ClubMember club in ClubMember)
            {
                AddClubMember(club);
            }
        }

        List<BL_backend.Transaction> IDAL.GetTransactionList()
        {
            List<BL_backend.Transaction> tranList = new List<BL_backend.Transaction>();
  
            foreach (Transaction t in db.Transactions)      //find transation
            {

                List<BL_backend.Transaction.SoldProducts> solp = new List<BL_backend.Transaction.SoldProducts>();
                Dictionary<int, BL_backend.Transaction.SoldProducts> soldPro2 = new Dictionary<int, BL_backend.Transaction.SoldProducts>();
                BL_backend.Transaction toAdd = new BL_backend.Transaction();


                BL_backend.PaymentMethod method = new BL_backend.PaymentMethod();

                string pay = t.PaymentMethod.type;
                switch (pay)
                {
                    case "Cash": method = BL_backend.PaymentMethod.Cash;
                        break;
                    case "Visa": method = BL_backend.PaymentMethod.Visa;
                        break;
                    case "Check": method = BL_backend.PaymentMethod.Check;
                        break;
                    case "Isracard": method = BL_backend.PaymentMethod.Isracard;
                        break;
                    case "MasterCard": method = BL_backend.PaymentMethod.MasterCard;
                        break;
                    case "AmericanExpress": method = BL_backend.PaymentMethod.AmericanExpress;
                        break;
                    case "Other": method = BL_backend.PaymentMethod.Other;
                        break;

                }
                toAdd = new BL_backend.Transaction(t.dateTime, t.isAReturn, method, soldPro2);
                toAdd.TransactionID = t.transactionID;



                foreach (SoldProduct sol in db.SoldProducts)
                {
                    if (sol.transactionID == toAdd.TransactionID)  //find sold product in the reciept
                    {


                        BL_backend.productTypes type = new productTypes();
                        string proType = sol.Product.ProductType.ToString();
                        switch (proType)
                        {
                            case "Cleaning": type = BL_backend.productTypes.Cleaning;
                                break;
                            case "Electronics": type = BL_backend.productTypes.Electronics;
                                break;
                            case "Furniture": type = BL_backend.productTypes.Furniture;
                                break;
                            case "None": type = BL_backend.productTypes.None;
                                break;
                        }
                        BL_backend.Product pro = new BL_backend.Product(sol.Product.name, type, sol.Product.inventoryID, sol.Product.location.Value, sol.Product.inStock.Value, sol.Product.stockCount.Value, sol.Product.price.Value);
                        BL_backend.Transaction.SoldProducts soltoAdd = new BL_backend.Transaction.SoldProducts(pro, sol.quantity);
                        solp.Add(soltoAdd);        //add sold product to the list 
                    }

                }
                for (int i = 0; i < solp.Count(); i++)     //adding sold product to the sold product to reciept
                {
                    soldPro2.Add(i, solp[i]);
                }
                toAdd.Receipt = soldPro2;

                tranList.Add(toAdd);
            }

            return tranList;
        }

        public void SetTransactionList(List<BL_backend.Transaction> Transaction)
        {
            foreach (Transaction tran in db.Transactions)
            {
                db.Transactions.DeleteOnSubmit(tran);


            }
            try
            {
                db.SubmitChanges();
            }
            catch
            {
                Errors err = new Errors();
                err.expRowAdd();
            }

            foreach (BL_backend.Transaction tran in Transaction)
            {
                AddTransaction(tran);
            }
        }

        public void SetDepartmentList(List<BL_backend.Department> Department)
        {
            foreach (Department dp in db.Departments)
            {
                db.Departments.DeleteOnSubmit(dp);


            }
            try
            {
                db.SubmitChanges();
            }
            catch
            {
                Errors err = new Errors();
                err.expRowAdd();
            }

            foreach (BL_backend.Department dp in Department)
            {
                AddDepartment(dp);
            }
        }

        List<BL_backend.Department> IDAL.GetDepartmentList()
        {
            List<BL_backend.Department> dp = new List<BL_backend.Department>();
            foreach (Department d in db.Departments)
            {
                BL_backend.Department toAdd = new BL_backend.Department(d.name, d.departmentID);
                dp.Add(toAdd);

            }
            return dp;
        }

        List<BL_backend.User> IDAL.GetUserList()
        {
            List<BL_backend.User> userList = new List<BL_backend.User>();
            
            foreach (User user in db.Users)
            {
                BL_backend.User.userType type = FindUserType(user.userName, user.password_);
   
                    BL_backend.User toAdd = new BL_backend.User(user.userName, user.password_, type);
                    userList.Add(toAdd);
           
            }
            return userList;
        }

        public void SetUserList(List<BL_backend.User> User)
        {
            foreach (User user in db.Users)
            {
                db.Users.DeleteOnSubmit(user);


            }
            try
            {
                db.SubmitChanges();
            }
            catch
            {
                Errors err = new Errors();
                err.expRowAdd();
            }

            foreach (BL_backend.User user in User)
            {
                AddUserList(user);
            }
        }

        public void AddProduct(BL_backend.Product P)
        {
            ProductType type = new ProductType(P.InventoryID1, P.Type.ToString());
            Product toAdd = new Product(P.Name, type, P.InventoryID1, P.Location, P.InStock1, P.StockCount, P.Price1);
            db.Products.InsertOnSubmit(toAdd);
            db.ProductTypes.InsertOnSubmit(type);
            try
            {
                db.SubmitChanges();
            }
            catch
            {
                Errors err = new Errors();
                err.expRowAdd();
            }
        }

        public void AddEmployee(BL_backend.Employee E)
        {
            Employee toAdd = new Employee(E.Salary, E.DepartmentID, E.Supervisor, E.teudatZeut, E.firstName, E.lastName, E.gender);
            db.Employees.InsertOnSubmit(toAdd);
            try
            {
                db.SubmitChanges();
            }
            catch
            {
                Errors err = new Errors();
                err.expRowAdd();
            }
        }

        public void AddClubMember(BL_backend.ClubMember C)
        {

   
            foreach (string s in C.transactionID)
            {
                TransactionIdList tranList = new TransactionIdList(s, C.memberID);
                db.TransactionIdLists.InsertOnSubmit(tranList);
            }
            userType type = new userType(C.user.UserName,C.user.Type.ToString());
            User newUser = new User(C.user.UserName,C.user.Password,type);
            ClubMember toAdd = new ClubMember(C.memberID, C.DateOfBirth, C.TeudatZeut, C.firstName, C.lastName, C.gender, newUser, C.CreditCardExpiration, C.CreditCardNumber, C.CreditCardThreeDigits);
            db.Users.InsertOnSubmit(newUser);
            db.userTypes.InsertOnSubmit(type);
            db.ClubMembers.InsertOnSubmit(toAdd);
            try
            {
                db.SubmitChanges();
            }
            catch
            {
                Errors err = new Errors();
                err.expRowAdd();
            }
        
        }

        public void AddTransaction(BL_backend.Transaction T)
        {
           
            PaymentMethod pay = new PaymentMethod(T.PaymentMethod.ToString());
            Transaction toAdd = new Transaction ();
            toAdd.transactionID = T.TransactionID;
            toAdd.dateTime = T.DateTime;
            toAdd.isAReturn = T.IsAReturn;
            toAdd.PaymentMethod = pay;
            pay.transactionID = T.TransactionID;
            
           // this._receipts = new System.Data.Linq.EntitySet<receipt>();
            for (int i = 0; i < T.Receipt.Count; i++)
            {
                //receipt[i].transactionID = this.transactionID;
                SoldProduct sol = new SoldProduct();
                sol.inventoryID = T.Receipt[i].ID1;
                sol.price = T.Receipt[i].Price;
                sol.quantity = T.Receipt[i].Quantity;
                sol.transactionID = T.TransactionID;
                db.SoldProducts.InsertOnSubmit(sol);
            }
            db.Transactions.InsertOnSubmit(toAdd);
            db.PaymentMethods.InsertOnSubmit(pay);
            try
            {
                db.SubmitChanges();
            }
            catch
            {
                Errors err = new Errors();
                err.expRowAdd();
            }
            
        }

        public void AddUserList(BL_backend.User U)
        {
            userType type = new userType(U.UserName, U.Type.ToString());
            User toAdd = new User(U.UserName, U.Password, type);
            db.Users.InsertOnSubmit(toAdd);
            db.userTypes.InsertOnSubmit(type);
            try
            {
                db.SubmitChanges();
            }
            catch
            {
                Errors err = new Errors();
                err.expRowAdd();
            }
        }

        public void AddDepartment(BL_backend.Department D)
        {
            Department toAdd = new Department(D.Name, D.DepartmentID);
            db.Departments.InsertOnSubmit(toAdd);
            try
            {
                db.SubmitChanges();
            }
            catch
            {
                Errors err = new Errors();
                err.expRowAdd();
            }
        }

        public void RemoveUserList(BL_backend.User U)
        {
            foreach (userType ty in db.userTypes)
            {
                if (ty.userName == U.UserName)
                {
                    db.userTypes.DeleteOnSubmit(ty);
                }
            }

            foreach (User u in db.Users)
            {
                if (u.userName == U.UserName)
                {
                    db.Users.DeleteOnSubmit(u);
                }
            }

    

            try
            {
                db.SubmitChanges();
            }
            catch
            {
                Errors err = new Errors();
                err.expRowRemove();
            }
        }

        public BL_backend.ClubMember FindClubMemberByMemberId(int memberId)
        {
              BL_backend.ClubMember newClub = null;
            foreach (ClubMember C in db.ClubMembers)
            {
                if (C.memberID == memberId)
                {
                    BL_backend.User newUser = new BL_backend.User(C.User.userName, C.User.password_, BL_backend.User.userType.ClubMember);
                    newClub = new BL_backend.ClubMember(C.memberID, C.dateOfBirth.Value, C.Person.teudatZeut, C.Person.firstName, C.Person.lastName, C.Person.gender, newUser, C.creditCardExpiration.Value, C.creditCardNumber.Value, C.creditCardThreeDigits.Value);
                }
            }
            return newClub;
        }
    }
}
