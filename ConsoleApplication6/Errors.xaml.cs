﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SQL_DB
{
    /// <summary>
    /// Interaction logic for Errors.xaml
    /// </summary>
    public partial class Errors : UserControl
    {
        public Errors()
        {
            InitializeComponent();
        }

        internal void expRowAdd()
        {
            //for adding
            MessageBox.Show("Value Already Exists (Duplicate Primary Key) ");
            throw new Exception();

        }
        internal void expRowRemove()
        {
            //for deleting
            MessageBox.Show("Cannot remove from Data Base");
            throw new Exception();
        }


         
    }
}
