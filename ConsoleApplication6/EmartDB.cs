using System;
using System.Collections.Generic;
namespace SQL_DB
{
    partial class Employee
    {

        public Employee(double salary, int departmentID, int supervisor, int teudatZeut, string firstName, string lastName, string gender)
        {
            this.Person = new Person();
            this.salary = salary;
            this.departmentID = departmentID;
            this.supervisor = supervisor;
            this.Person.teudatZeut = teudatZeut;
            this.Person.firstName = firstName;
            this.Person.lastName = lastName;
            this.Person.gender = gender;
        }
    }

    partial class Product
    {


        public Product(Product product)
        {
            this.name = product.name;
            this.ProductType = product.ProductType;
            this.inventoryID = product.inventoryID;
            this.location = product.location;
            this.inStock = product.inStock;
            this.stockCount = product.stockCount;
            this.price = product.price;
        }



        public Product(string name, ProductType type, int inventoryID, int location, Boolean inStock, int stockCount, double price)
        {
            this.name = name;
            this.ProductType = type;
            this.ProductType.type = type.type;
            this.inventoryID = inventoryID;
            this.location = location;
            this.inStock = inStock;
            this.stockCount = stockCount;
            this.price = price;
            this._SoldProducts = new System.Data.Linq.EntitySet<SoldProduct>();
            
        }

    
    }

    partial class ProductType
    {
        public ProductType(int inventoryId, string kind)
        {
            this.inventoryID = inventoryId;
            this.type = kind;
        }
    
    }

    partial class User
    { 
      
        public User(string userName, int password, userType type)
        {
           
            this.userName = userName;
            this.password_ = password;
            this.userType = type;
            this.userType.kind = type.kind;
            this._ClubMembers = new System.Data.Linq.EntitySet<ClubMember>();
            
            
        }
    
    }

    partial class userType
    {
        public userType(string userName, string kind)
        {
            this.kind = kind;
            this.userName = userName;
        }
    
    }

    partial class Department
    {
        public Department(string name, int dpId)
        {
            this.departmentID = dpId;
            this.name = name;
        }
    
    }

    partial class SoldProduct
    {
        public SoldProduct(Product product, int quantity)
        {
            this.quantity = quantity;
            this.price = product.price.Value;
            this.inventoryID = product.inventoryID;
          

        
        }
    
    }

    partial class ClubMember
    {
        public ClubMember(int memberID, DateTime dateOfBirth, int teudatZeut, string firstName, string lastName, string gender, User user, DateTime creditCardExpiration, long creditCardNumber, int creditCardThreeDigits)
          
        {
            //transactionID = new List<string>();
            this.Person = new Person();
            this.Person.teudatZeut = teudatZeut;
            this.Person.firstName = firstName;
            this.Person.lastName = lastName;
            this.Person.gender = gender;
            this.memberID = memberID;
            this.dateOfBirth = dateOfBirth;
            this.User = user;
            this.creditCardExpiration = creditCardExpiration;
            this.creditCardNumber = creditCardNumber;
            this.creditCardThreeDigits = creditCardThreeDigits;
            this._TransactionIdLists = new System.Data.Linq.EntitySet<TransactionIdList>();
            
            
        }
    
    }

    partial class PaymentMethod
    {
        public PaymentMethod( string type)
        {
                                           
            this._type = type;
        }
    
    }


    partial class Transaction
    {
        public Transaction(DateTime dateTime, Boolean isAReturn, PaymentMethod paymentMethod,List<SoldProduct> sol)
        {
            this.transactionID = System.Guid.NewGuid().ToString();
            this.dateTime = dateTime;
            this.isAReturn = isAReturn;
            this.PaymentMethod = paymentMethod;
            paymentMethod.transactionID = this.transactionID;
            this._SoldProducts = new System.Data.Linq.EntitySet<SoldProduct>();
            
           // this._receipts = new System.Data.Linq.EntitySet<receipt>();
            for (int i = 0; i < sol.Count; i++)
            {
                //receipt[i].transactionID = this.transactionID;
                sol[i].transactionID = this.transactionID;
                
               
            }
                       

        }
    
    }


    partial class TransactionIdList
    {
        public TransactionIdList(string transactionId, int memberId)
        {
            this.transactionID = transactionId;
            this._transactionID = transactionId;
            this.memberID = memberId;
            this._memberID = memberId;
        }
    }
            
}
