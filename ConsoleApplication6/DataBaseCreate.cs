﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using BL_backend;
using System.IO;



namespace SQL_DB
{


    class DataBaseCreate
    {
       

        public DataBaseCreate()

        {
             EmartDBDataContext db = new EmartDBDataContext();
             
            if (db.Employees.Count()==0)
            {
                List<Employee> employees = new List<Employee>();
                employees.Add(new Employee(4500, 141510000, 305509069, 305473488, "David", "Choen", "Male"));
                employees.Add(new Employee(4300, 141610000, 304789451, 306472488, "Nir", "Barnes", "Male"));
                employees.Add(new Employee(4000, 140510000, 304789451, 304789111, "Bar", "Zaguri", "Female"));
                employees.Add(new Employee(4000, 140510000, 304780001, 304789451, "Racheli", "Zaguri", "Female"));
                employees.Add(new Employee(10000, 140510000, 305509006, 304789451, "Yarden", "Chen", "Female"));
                db.Employees.InsertAllOnSubmit(employees);
                db.SubmitChanges();    
            
            }

            if (db.Departments.Count() == 0)
            {
                List<Department> departments = new List<Department>();
                departments.Add(new Department("Warehousemans", 141450000));
                departments.Add(new Department("Furniture Salon", 141610000));
                departments.Add(new Department("Cashiers", 140510000));
                db.Departments.InsertAllOnSubmit(departments);
                db.SubmitChanges();
            }
            if (db.Users.Count()==0)
            {

               
                List<User> users = new List<User>();
                users.Add(new User("Idan", 1234, new userType("Idan", "Admin")));
                users.Add(new User("Yarden", 4321,new userType("Idan", "Admin")));
                users.Add(new User("Racheli", 1111, new userType("Racheli", "Worker")));
                users.Add(new User("Avi", 2222, new userType("Avi", "ClubMember")));
                db.Users.InsertAllOnSubmit(users);
                db.SubmitChanges();  

            

            
            
                List<userType> types = new List<userType>();
                types.Add(new userType("Idan", "Admin"));
                types.Add(new userType("Idan", "Admin"));
                types.Add(new userType("Racheli", "Worker"));
                types.Add(new userType("Avi", "ClubMember"));
              
            }
            
            
           



             if (db.Products.Count()==0)
             {
                List<Product> products = new List<Product>();
                products.Add(new Product("TV LED 32'' ", new ProductType(301550906,"Electronics") ,301550906, 401510000, true, 500, 1500));
                products.Add(new Product("Broom", new ProductType(301610000,"Cleaning"), 301610000, 401610000, true, 350, 20));
                products.Add(new Product("Couch 3 Seats", new ProductType(301710000,"Furniture"), 301710000, 401710000, true, 500, 1500));
                db.Products.InsertAllOnSubmit(products);
                db.SubmitChanges();  

             }


       


            
            if (db.Transactions.Count()==0)
            {
                List<SoldProduct> allSol = new List<SoldProduct>();
                //List<receipt> allReceipt = new List<receipt>();
                List<Product> products = new List<Product>();
                SQL_DAL itDal = new SQL_DAL();
                products = itDal.GetProductList();
                int count = products.Count();

                
                List<Transaction> transactions = new List<Transaction>();
                List <SoldProduct> sol = new List<SoldProduct>();
                sol.Add(new SoldProduct(products[count - 1], 1));
                //List <receipt> soldPro = new List <receipt>();
                //soldPro.Add(new receipt(1));
                transactions.Add(new Transaction(new DateTime(2015, 4, 15), false, new PaymentMethod("Cash"),sol));
                allSol.AddRange(sol);
                ///allReceipt.AddRange(soldPro);
                count--;
                
                if (count >= 0)
                {
                    List<SoldProduct> sol1 = new List<SoldProduct>();
                    sol1.Add(new SoldProduct(products[count], 3));
                    //List <receipt> soldPro1 = new List <receipt>();
                    //soldPro1.Add(new receipt(1));
                    transactions.Add(new Transaction(new DateTime(2015, 4, 18), false,new PaymentMethod("Visa"),sol1));
                    allSol.AddRange(sol1);
                    //allReceipt.AddRange(soldPro1);
                    count--;
                }
                if (count >= 0)
                {
                    List<SoldProduct> sol2 = new List<SoldProduct>();
                    sol2.Add(new SoldProduct(products[count+1], 2));
                    sol2.Add(new SoldProduct(products[count], 3));
                   // List<receipt> soldPro2 = new List<receipt>();
                    //soldPro2.Add(new receipt(1));
                    //soldPro2.Add(new receipt(2));
                   // receipt soldPro2 = new receipt { { 1, sol }, { 2, sol2 } };
                    transactions.Add(new Transaction(new DateTime(2015, 4, 12), false, new PaymentMethod("Check") , sol2));
                    allSol.AddRange(sol2);
                    //allReceipt.AddRange(soldPro2);
                }
                
              ;
                db.Transactions.InsertAllOnSubmit(transactions);
                db.SoldProducts.InsertAllOnSubmit(allSol);
                //db.receipts.InsertAllOnSubmit(allReceipt);
                db.SubmitChanges();  
            

            }

            if (db.ClubMembers.Count()==0)
            {
                List<ClubMember> clubMembers = new List<ClubMember>();
                clubMembers.Add(new ClubMember(121212121, new DateTime(1984, 6, 14), 909090909, "Avi", "Liberman", "Male", new User("Avi", 5555,new userType("Avi", "ClubMember")), new DateTime(2015, 5, 1), 123456789111, 234));
                clubMembers.Add(new ClubMember(136570000, new DateTime(1965, 1, 1), 140987065, "Rachel", "Levi", "Female", new User("Rachel", 4444, new userType("Avi", "ClubMember")), new DateTime(2015, 5, 1), 123454545123, 155));
                clubMembers.Add(new ClubMember(111490000, new DateTime(1970, 4, 15), 200475982, "Ben", "Golan", "Male", new User("Ben", 9999, new userType("Avi", "ClubMember")), new DateTime(2015, 4, 1), 123451545115, 233));
                clubMembers.Add(new ClubMember(121450000, new DateTime(1966, 5, 3), 130258976, "Yakov", "Mor", "Male", new User("Yakov", 8888, new userType("Avi", "ClubMember")), new DateTime(2014, 1, 1), 1524516541222, 155));
                db.ClubMembers.InsertAllOnSubmit(clubMembers);
                db.SubmitChanges();  

            }

        }


    }
}
