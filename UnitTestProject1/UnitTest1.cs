﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BL;
using BL_backend;
using DAL;
using SQL_DB;
using NUnit;
using System.Collections.Generic;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
        static IDAL idal = new SQL_DAL();
        IBL blTest = new product_BL(idal);

        [TestMethod]
        public void AddProduct()
        {

            blTest.AddnewProduct("TV", BL_backend.productTypes.Electronics, 121200000, 141414141, true, 50, 150);
            Assert.AreEqual("TV", idal.FindProductByID(121200000).getName());
            blTest.deleteProduct(121200000);
           
            
        }

        [TestMethod]
        public void EditProduct()
        {

            blTest.AddnewProduct("TV", BL_backend.productTypes.Electronics, 121200000, 141414141, true, 50, 100);
            blTest.updateProduct("TV", 121200000, 150, BL_backend.productTypes.Electronics, 141414100, 150, 121200000, true); //edit the stock number of the product
            Assert.AreEqual(150, idal.FindProductByID(121200000).StockCount);
            blTest.deleteProduct(121200000);

        }

        [TestMethod]
        public void RemoveProduct()
        {
            blTest.AddnewProduct("TV", BL_backend.productTypes.Electronics, 121200000, 141414141, true, 50, 100);
            blTest.deleteProduct(121200000);
            Assert.AreEqual(null, idal.FindProductByID(121200000));  //the product doesn't exist so find product return t

        }

        /// <summary>
        /// *******New 7 NUnits****
        /// </summary>


        ///***Users
        [TestMethod]
        public void AddUser()
        { 
            blTest.addUser(new BL_backend.User("Israel",123456,BL_backend.User.userType.Admin));
            Assert.AreEqual(true, blTest.FindUser("Israel",123456));
            blTest.removeUser(new BL_backend.User("Israel", 123456, BL_backend.User.userType.Admin));
        }

        [TestMethod]
        public void EditUser()
        {
            blTest.addUser(new BL_backend.User("Israel", 123456, BL_backend.User.userType.Admin));
            blTest.updateUser("Israel", 1234, "Israel", BL_backend.User.userType.Manager);
            Assert.AreEqual("Manager", blTest.FindUserType("Israel",1234).ToString());
            blTest.removeUser(new BL_backend.User("Israel", 1234, BL_backend.User.userType.Manager));
        }


        
    

        [TestMethod]
        public void RemoveUser()
        {
            blTest.addUser(new BL_backend.User("Israel", 123456, BL_backend.User.userType.Admin));
            blTest.removeUser(new BL_backend.User("Israel", 123456, BL_backend.User.userType.Admin));
            Assert.AreEqual(false, blTest.FindUser("Israel", 123456));
        }



        /// <summary>
        /// ***Departments
        /// </summary>
        [TestMethod]
        public void AddDepartment()
        {
            blTest.AddNewDepartment(789456321, "Kitchen");

            Assert.AreEqual(true, blTest.FindDepartmentByID(789456321));

            blTest.deleteDepartment(789456321);
        }

        [TestMethod]
        public void EditDepartment()
        {
            blTest.AddNewDepartment(789456321, "Kitchen");
            blTest.updateDepartment(789456321, "Animals", 789456321);

            Assert.AreEqual(789456321, blTest.GetDepartmentByName("Animals")[0].DepartmentID);

            blTest.deleteDepartment(789456321);
        }


        [TestMethod]
        public void RemoveDepartment()
        {
            blTest.AddNewDepartment(789456321, "Kitchen");
            blTest.deleteDepartment(789456321);

            Assert.AreEqual(false, blTest.FindDepartmentByID(789456321));

          
        }

        //**Employee
        [TestMethod]
        public void AddEmployee()
        {
           
            blTest.AddNewEmployee(2500, 123456789, 987654321,301456987, "Ben", "Koglar", "Male");
            Assert.AreEqual(true, blTest.FindEmployeeByID(301456987));
            Assert.AreEqual("Ben", idal.FindEmployeeByID(301456987).getFirstName());
            blTest.deleteEmployee(301456987);
        }

        [TestMethod]
        public void EditEmployee()
        {

            blTest.AddNewEmployee(2500, 123456789, 987654321, 301456987, "Ben", "Koglar", "Male");
            blTest.updateEmployee(301456987, "Ben Zion", 3000, "Koglar", "Male", 301456987, 123456789, 987654321);
            Assert.AreEqual("Ben Zion", idal.FindEmployeeByID(301456987).getFirstName());
            Assert.AreEqual(3000, idal.FindEmployeeByID(301456987).Salary);
            blTest.deleteEmployee(301456987);
        }


 

        [TestMethod]
        public void RemoveEmployee()
        {

            blTest.AddNewEmployee(2500, 123456789, 987654321, 301456987, "Ben", "Koglar", "Male");
            blTest.deleteEmployee(301456987);
            Assert.AreEqual(false, blTest.FindEmployeeByID(301456987));
        
        }


        [TestMethod]
        public void AddClubMember()
        {

            blTest.AddnewClubMember(123654777, new DateTime(1970, 08, 07), 300088976, "Ben David", "Golan", "Male", new BL_backend.User("Ben David", 1234, BL_backend.User.userType.ClubMember), new DateTime(2018, 05, 05), 123456789963, 123);
            Assert.AreEqual(true, blTest.FindClubMemberByID(300088976));
            blTest.deleteClubMember(300088976);

        }




        /// <summary>
        /// *******CHECK SQL PRIMERY KEY****
        /// </summary>
        /// 


        [TestMethod]
        public void addDuplicateUser()
        {
            blTest.addUser(new BL_backend.User("Roni", 1234, BL_backend.User.userType.Admin));
            bool check = false;
            try
            {
                blTest.addUser(new BL_backend.User("Roni", 1234, BL_backend.User.userType.Admin));
            }
            catch
            {
                check = true;
            }
            blTest.removeUser(new BL_backend.User("Roni", 1234, BL_backend.User.userType.Admin));
            blTest.removeUser(new BL_backend.User("Roni", 1234, BL_backend.User.userType.Admin));
            Assert.AreEqual(true, check);
        }

        [TestMethod]
        public void addDuplicateEmployee()
        {
            blTest.AddNewEmployee(2500, 123456000, 123456000, 898989898, "Ben", "Koglar", "Male");
            bool check = false;
            try
            {
                blTest.AddNewEmployee(2500, 123456000, 123456000, 898989898, "Ben", "Koglar", "Male");
            }
            catch
            {
                check = true;
            }
            
            Assert.AreEqual(true, check);
            blTest.deleteEmployee(898989898);
            blTest.deleteEmployee(898989898);

        }

        [TestMethod]
        public void addDuplicateProduct()
        {

            blTest.AddnewProduct("IPHONE6", BL_backend.productTypes.Electronics, 305500011, 305500000, true, 250, 150);
            bool check = false;
            try
            {
                blTest.AddnewProduct("IPHONE6", BL_backend.productTypes.Electronics, 305500011, 305500000, true, 250, 150);

            }
            catch
            {
                check = true;
                
            }
            blTest.deleteProduct(305500011);
            blTest.deleteProduct(305500011);
            Assert.AreEqual(true, check);
        }

        [TestMethod]
        public void addDuplicateDepartment()
        {

            blTest.AddNewDepartment(789456111, "Kitchen");
            bool check = false;
            try
            {
                blTest.AddNewDepartment(789456111, "Kitchen");
            }
            catch
            {
                check = true;
            }
            blTest.deleteDepartment(789456111);
            blTest.deleteDepartment(789456111);
            Assert.AreEqual(true, check);
        }

    }
}
