﻿using BL_backend;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace GUI
{
    class ProductListItem : ContentControl
    {
        public string ProductName { get; set; }

        public productTypes Type { get; set; }

        public int InventoryID { get; set; }

        public int Location { get; set; }

        public bool InStock { get; set; }

        public int StockCount { get; set; }

        public double Price { get; set; }

        public Product Product { get; set; }

        public ProductListItem(Product product)
        {
            ProductName = product.getName();
            InStock = product.InStock1;
            InventoryID = product.getID();
            Location = product.getLocation();
            Price = product.getPrice();
            StockCount = product.getSC();
            Type = product.getType();
            Product = product;
        }

        public ProductListItem(ProductListItem other)
        {
            ProductName = other.ProductName;
            this.Type = other.Type;
            this.InventoryID = other.InventoryID;
            this.Location = other.Location;
            this.InStock = other.InStock;
            this.StockCount = other.StockCount;
            this.Price = other.Price;
            this.Product = other.Product;
        }
    }
}
