﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BL;
using BL_backend;

namespace GUI
{
    /// <summary>
    /// Interaction logic for User_Edit.xaml
    /// </summary>
    public partial class User_Edit : Window
    {
        IBL itsBL;
        User toEdit;
        string LuserName;
        Window last;
        public User_Edit(User user,IBL bl, Window last)
        {
            this.last = last;
            itsBL = bl;
            toEdit = user;
            LuserName = user.UserName;
            InitializeComponent();

            UserName.Text = toEdit.UserName;
            pass.Text = ""+toEdit.Password;
            Type.Text = "" + toEdit.Type;
        }

        private void Type_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                toEdit.UserName = UserName.Text;
                toEdit.Password = Convert.ToInt32(pass.Text);
                toEdit.Type = (User.userType)Enum.Parse(typeof(User.userType), Type.Text, true);
                itsBL.updateUser(LuserName, toEdit.Password, toEdit.UserName, toEdit.Type);
            }

            catch (Exception)
            {
                MessageBox.Show("Invalid Input");
                return;
            }

           
            MessageBox.Show("The User was Updated Successfully");

        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            last.Show();
            this.Close();
        }
    }
}
