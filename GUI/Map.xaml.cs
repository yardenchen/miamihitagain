﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BL;
using BL_backend;

namespace GUI
{
    /// <summary>
    /// Interaction logic for map.xaml
    /// </summary>
    public partial class Map : Window
    {
       
        private IBL itsBL;
        private User U;
        public string town;
        public Map(string town, IBL IBL, User U)
        {
            this.itsBL = IBL;
            this.U = U;
           
            this.town = town;
            InitializeComponent();
        }

        private void WebBrowser_Loaded(object sender, RoutedEventArgs e)
        {
            if (town != string.Empty)
            {
                google.Navigate("http://maps.google.com/maps?q=" + town);
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Stores s = new Stores(itsBL, U);
            s.Show();
            this.Close();
        }
    }
}