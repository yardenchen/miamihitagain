﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BL;
using BL_backend;

namespace GUI
{
    /// <summary>
    /// Interaction logic for Department_Edit.xaml
    /// </summary>
    public partial class Department_Edit : Window
    {
        Department toEdit;
        IBL itsBL;
        int LdpId;
        Window last;
        public Department_Edit(Department dp, IBL bl, Window last)
        {
            this.last = last;
            itsBL = bl;
            toEdit = dp;
            LdpId = dp.DepartmentID;
            InitializeComponent();
            Name.Text = toEdit.Name;
            dpId.Text = "" + toEdit.DepartmentID;

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                toEdit.DepartmentID = Convert.ToInt32(dpId.Text);
                toEdit.Name = Name.Text;
                itsBL.updateDepartment(LdpId, toEdit.Name, toEdit.DepartmentID);
            }

            catch(Exception)
            {
                MessageBox.Show("Invalid Input");
                return;
            }

            
            MessageBox.Show("The Department was Updated Successfully");
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            last.Show();
            this.Close();
        }


    }
}
