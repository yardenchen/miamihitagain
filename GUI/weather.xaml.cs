﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BL;
using BL_backend;
using System.Xml;

namespace GUI
{

    public partial class weather : Window
    {
        private IBL itsBL;
        private User U;
       
        string town;
        string tempature;
        string humidity;
        string TFHigh;
        string TFLow;
        string oweid;
        string wind;
        public weather(string oweid, IBL IBL, User U)
        {
            this.itsBL = IBL;
            this.U = U;

            this.oweid = oweid;
            InitializeComponent();
        }
        private void GetWeather()
        {
            string query = string.Format("http://weather.yahooapis.com/forecastrss?w=" + oweid);
            XmlDocument wdata = new XmlDocument();
            wdata.Load(query);

            XmlNamespaceManager manager = new XmlNamespaceManager(wdata.NameTable);
            manager.AddNamespace("yweather", "http://xml.weather.yahoo.com/ns/rss/1.0");

            XmlNode channel = wdata.SelectSingleNode("rss").SelectSingleNode("channel");
            XmlNodeList nodes = wdata.SelectNodes("rss/channel/item/yweather:forcast", manager);
            tempature = (string)channel.SelectSingleNode("item").SelectSingleNode("yweather:condition", manager).Attributes["temp"].Value;
            humidity = channel.SelectSingleNode("yweather:atmosphere", manager).Attributes["humidity"].Value;
            wind = channel.SelectSingleNode("yweather:wind", manager).Attributes["speed"].Value;
            town = channel.SelectSingleNode("yweather:location", manager).Attributes["city"].Value;
            TFHigh = channel.SelectSingleNode("item").SelectSingleNode("yweather:forecast", manager).Attributes["high"].Value;
            TFLow = channel.SelectSingleNode("item").SelectSingleNode("yweather:forecast", manager).Attributes["low"].Value;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            GetWeather();
            txtTown.Text = town;
            txtHum.Text = humidity;
            txtTemp.Text = tempature;
            txtHT.Text = TFHigh;
            txtLT.Text = TFLow;
            txtWind.Text = wind;
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Stores s = new Stores(itsBL, U);
            s.Show();
            this.Close();
        }

        private void Button_Loaded(object sender, RoutedEventArgs e)
        {
            GetWeather();
            txtTown.Text = town;
            txtHum.Text = humidity;
            txtTemp.Text = tempature;
            txtHT.Text = TFHigh;
            txtLT.Text = TFLow;
            txtWind.Text = wind;
        }
    }
}