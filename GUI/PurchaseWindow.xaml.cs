﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BL;
using DAL;
using BL_backend;
using System.Threading;
using System.Collections;
using System.ComponentModel;

namespace GUI
{
    /// <summary>
    /// Interaction logic for PurchaseWindow.xaml
    /// </summary>
    public partial class PurchaseWindow : Window
    {
        int count;

        System.Windows.Threading.DispatcherTimer dispatcherTimer;

        IBL _ibl;
        User _user;

        //public PurchaseWindow()
        //{
        //    InitializeComponent();
        //   // _ibl = new product_BL(new LINQ_DAL());
        //    //  _user = new User("Avi", 2222, User.userType.Customer);
        //    //_user = new User("Idan", 1234, User.userType.Admin);
        //    // _user = new User("Avi", 5555, User.userType.ClubMember);
        //    _user = new User("Rachel", 4444, User.userType.ClubMember);
        //    InitWindow(_ibl, _user);

        //}

        public PurchaseWindow(IBL ibl, User user)
        {
            InitializeComponent();
            InitWindow(ibl, user);

        }

        private void InitWindow(IBL ibl, User user)
        {
            this._ibl = ibl;
            this._user = user;
            string[] productTypes = Enum.GetNames(typeof(productTypes));
            cmbType.ItemsSource = productTypes;
            string[] paymentMethod = Enum.GetNames(typeof(PaymentMethod));
            cmbPay.ItemsSource = paymentMethod;
            LoadBestSeller();
            List<Product> productList = _ibl.GetAllProductsList();
            LoadProducts(productList);
            InitControls(_user.Type);

        }

        private void InitControls(BL_backend.User.userType userType)
        {
            btnAdminPurchase.IsEnabled = false;
            btnPay.IsEnabled = false;
            btnPayAndSave.IsEnabled = false;
            switch (userType)
            {
                case User.userType.Admin:
                    btnAdminPurchase.IsEnabled = true;
                    break;
                case User.userType.Manager:
                    break;
                case User.userType.Worker:
                    break;
                case User.userType.Customer:
                    btnPay.IsEnabled = true;
                    break;
                case User.userType.ClubMember:
                    btnPay.IsEnabled = true;
                    break;
                default:
                    break;
            }
        }

        private void LoadProducts(List<Product> productList)
        {
            ListProduct.Items.Clear();
            if (productList == null)
            {
                productList = _ibl.GetAllProductsList();
            }
            count = productList.Count;
            prgBar.Maximum = productList.Count;
            //prgBar.Minimum = 0;
            //prgBar.Maximum = 100;
            BackgroundWorker worker = new BackgroundWorker();
            worker.WorkerReportsProgress = true;
            worker.DoWork += worker_DoWork;
            worker.ProgressChanged += worker_ProgressChanged;

            worker.RunWorkerAsync(productList.Count);

            foreach (Product product in productList)
            {
                ProductListItem item = new ProductListItem(product);
                ListProduct.Items.Add(item);
            }

        }

        void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            prgBar.Value = e.ProgressPercentage;
            lblProg.Content = Math.Round((prgBar.Value) / prgBar.Maximum, 1) * 100 + "%";
        }

        void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            int total = (int)e.Argument;
            for (int i = 1; i < total + 1; i++)
            {
                (sender as BackgroundWorker).ReportProgress(i);
                Thread.Sleep(500);
            }
        }

        private void LoadBestSeller()
        {
            Product product = _ibl.GetBestSeller(4);
            if (product != null)
            {
                lblBestSeller.Content = product.getName();
            }
        }

        private void ListCart_Drop(object sender, DragEventArgs e)
        {

            // If the DataObject contains string data, extract it. 
            if (e.Data.GetDataPresent(typeof(ProductListItem)))
            {
                ProductListItem dataItem = (ProductListItem)e.Data.GetData(typeof(ProductListItem));
                ListCart.Items.Add(new ProductListItem(dataItem));

            }

        }

        private void ListProduct_MouseMove(object sender, MouseEventArgs e)
        {
            ProductListItem item = ListProduct.SelectedItem as ProductListItem;
            if (item != null && e.LeftButton == MouseButtonState.Pressed)
            {
                DragDrop.DoDragDrop(item,
                                     item,
                                     DragDropEffects.Copy);
            }
        }

        private void btnSearchName_Click(object sender, RoutedEventArgs e)
        {

            Product product = _ibl.GetProductByName(txtName.Text);
            ListProduct.Items.Clear();
            if (product != null)
            {
                ProductListItem item = new ProductListItem(product);

                ListProduct.Items.Add(item);
            }
            else if (String.IsNullOrEmpty(txtName.Text))
            {
                List<Product> productList = _ibl.GetAllProductsList();
                LoadProducts(productList);
            }
        }

        private void txtName_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            txtName.Text = "";
        }

        private void txtID_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            txtID.Text = "";
        }



        private void cmbType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            productTypes type = (productTypes)Enum.Parse(typeof(productTypes), (string)cmbType.SelectedItem);

            List<Product> productList = _ibl.GetProductListByType(type);
            LoadProducts(productList);
        }


        private void txtThreeDigits_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            txtThreeDigits.Text = "";

        }

        private void txtCardNumber_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            txtCardNumber.Text = "";

        }

        private void btnSearchByID_Click(object sender, RoutedEventArgs e)
        {
            if (String.IsNullOrEmpty(txtID.Text))
            {
                List<Product> productList = _ibl.GetAllProductsList();
                LoadProducts(productList);
            }
            else
            {
                int n;

                bool success = int.TryParse(txtID.Text, out n);
                if (success)
                {
                    Product product = _ibl.GetProductByID(n);

                    ListProduct.Items.Clear();
                    if (product != null)
                    {
                        ProductListItem item = new ProductListItem(product);

                        ListProduct.Items.Add(item);
                    }
                }
                else
                {
                    MessageBox.Show("Invalid input");
                }
            }
        }


        private void btnPayAndSave_Click(object sender, RoutedEventArgs e)
        {

            ClubMember clubMember = _ibl.GetClubMemberByUser(_user);
            string a = clubMember.firstName;

            if (clubMember != null && GetCreditDetails())
            {

                DateTime dateExp = new DateTime(int.Parse(txtYear.Text), int.Parse(txtMonth.Text), 1);
                clubMember.CreditCardExpiration = dateExp;
                long number = long.Parse(txtCardNumber.Text);
                clubMember.CreditCardNumber = number;
                int threeDig = int.Parse(txtThreeDigits.Text);
                clubMember.CreditCardThreeDigits = threeDig;
                _ibl.updateClubMember(clubMember.teudatZeut, clubMember.teudatZeut, clubMember.getFirstName(),
                clubMember.getLasttName(), clubMember.getDateOfBirth(), clubMember.getGender(),
                clubMember.memberID, clubMember.CreditCardExpiration, clubMember.CreditCardNumber,
                clubMember.CreditCardThreeDigits);
                PaymentMethod payment = (PaymentMethod)Enum.Parse(typeof(PaymentMethod), (string)cmbPay.SelectedItem);
                Payment(payment, true, false);
            }
        }

        private Dictionary<int, Transaction.SoldProducts> GetReciept(bool isAReturn)
        {
            bool flag = false;
            int key = 0;
            Dictionary<int, Transaction.SoldProducts> reciept = new Dictionary<int, Transaction.SoldProducts>();
            foreach (ProductListItem item in ListCart.Items)
            {
               
                int quantity = GetQuantity(item);
                if (quantity > 0 && quantity <= item.StockCount)
                {
                    Transaction.SoldProducts soldProd = new Transaction.SoldProducts(item.Product, quantity);
                    if (isAReturn)
                    {
                        soldProd.Price = -soldProd.Price;
                    }
                  //  reciept.Add(item.InventoryID, soldProd);
                    reciept.Add(key, soldProd);
                    key++;
                    int stockCount;
                    if (isAReturn)
                    {
                        stockCount = item.StockCount + quantity;
                    }
                    else
                    {
                        stockCount = item.StockCount - quantity;
                    }
                    _ibl.updateProduct(item.ProductName, item.InventoryID, stockCount, item.Type, item.Location, item.Price, item.InventoryID, true);
                    flag = true;
                }
                else
                {
                    MessageBox.Show("Invalid Quantity");
                }

            }
            if (flag)
            {
                return reciept;
            }
            else return null;
        }

        private double GetTotalPrice()
        {
            double result = 0;
            foreach (ProductListItem item in ListCart.Items)
            {
                int quantity = GetQuantity(item);
                result += quantity * item.Price;
            }
            return result;
        }

        private int GetQuantity(ProductListItem ListCartItem)
        {
            var gridView = ListCart.View as GridView;
            ListBoxItem myListBoxItem = ListCart.ItemContainerGenerator.ContainerFromItem(ListCartItem) as ListBoxItem;

            //ContentControl does not directly host the elements provided by the expanded DataTemplate.  
            //It uses a ContentPresenter to do the  work. 
            //If we want to use the FindName method we will have to pass that ContentPresenter as the second argument, instead of the ContentControl. 
            ContentPresenter myContentPresenter = GetVisualChild<ContentPresenter>(myListBoxItem);

            var grid = gridView.Columns[2].CellTemplate.FindName("txtListCartQuantity", myContentPresenter);
            string text = ((System.Windows.Controls.TextBox)(grid)).Text;

            int quantity;
            bool success = int.TryParse(text, out quantity);
            if (success)
                return quantity;
            return -1;
        }

        private T GetVisualChild<T>(Visual parent) where T : Visual
        {
            T child = default(T);

            for (int index = 0; index < VisualTreeHelper.GetChildrenCount(parent); index++)
            {
                Visual visualChild = (Visual)VisualTreeHelper.GetChild(parent, index);
                child = visualChild as T;

                if (child == null)
                    child = GetVisualChild<T>(visualChild);//Find Recursively

                else
                    return child;
            }
            return child;
        }



        private bool GetCreditDetails()
        {

            bool ans;
            bool successNum = false;
            bool successThree = false;
            int month;
            int year;
            bool monthSucc = int.TryParse(txtMonth.Text, out month);
            bool yearSucc = int.TryParse(txtYear.Text, out year);
            bool successDate = monthSucc && yearSucc;


            if (monthSucc && yearSucc)
            {
                if (int.Parse(txtMonth.Text) < 1 || int.Parse(txtMonth.Text) > 12)
                {
                    MessageBox.Show("Month is not valid");
                }
                if (int.Parse(txtYear.Text) < 2015)
                {
                    MessageBox.Show("Year is not valid");
                }
                else if (int.Parse(txtYear.Text) == 2015)
                {
                    if (int.Parse(txtMonth.Text) < DateTime.Now.Month)
                    {
                        MessageBox.Show("Card date has expired");
                    }
                }
            }
            else
            {
                MessageBox.Show("Invalid month or year");
            }
            long number;
            successNum = long.TryParse(txtCardNumber.Text, out number);
            if (successNum)
            {
                int cnt = 1;
                long n = long.Parse(txtCardNumber.Text);
                while (n / 10 > 0)
                {
                    n = n / 10;
                    cnt++;
                }
                if (cnt != 12)
                {
                    MessageBox.Show("Invalid Card Number");
                    successNum = false;
                }
                else
                {
                    successNum = true;
                }
            }
            int threeDig;
            successThree = int.TryParse(txtThreeDigits.Text, out threeDig);
            if (successThree)
            {
                int cnt2 = 1;
                int n2 = int.Parse(txtThreeDigits.Text);
                while (n2 / 10 > 0)
                {
                    n2 = n2 / 10;
                    cnt2++;
                }
                if (cnt2 != 3)
                {

                    MessageBox.Show("Invalid Card Three Digits");
                    successThree = false;
                }
                else
                {
                    successThree = true;
                }
            }

            return ans = successDate && successNum && successThree;
        }



        private void cmbPay_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            PaymentMethod payment = (PaymentMethod)Enum.Parse(typeof(PaymentMethod), (string)cmbPay.SelectedItem);
            if (_ibl.IsCreditCard(payment))
            {
                if (_user.Type == User.userType.ClubMember)
                {
                    btnPayAndSave.IsEnabled = true;
                }
                txtCardNumber.IsEnabled = true;
                txtYear.IsEnabled = true;
                txtMonth.IsEnabled = true;
                txtThreeDigits.IsEnabled = true;
            }
            else
            {
                btnPayAndSave.IsEnabled = false;
                txtCardNumber.IsEnabled = false;
                txtYear.IsEnabled = false;
                txtMonth.IsEnabled = false;
                txtThreeDigits.IsEnabled = false;
            }
        }

        private void btnViewSum_Click(object sender, RoutedEventArgs e)
        {
            if (ListCart.Items != null)
            {
                {
                    double price = GetTotalPrice();
                    MessageBox.Show("The Total Sum is " + price);
                }

            }
        }

        private void btnPay_Click(object sender, RoutedEventArgs e)
        {
            Pay();
        }

        private void Pay(bool isAReturn = false)
        {
            if (cmbPay.SelectedIndex != -1)
            {
                PaymentMethod payment = (PaymentMethod)Enum.Parse(typeof(PaymentMethod), (string)cmbPay.SelectedItem);
                if (_ibl.IsCreditCard(payment))
                {
                    if (GetCreditDetails())
                    {
                        Payment(payment, true, isAReturn);
                    }

                }
                else
                {
                    Payment(payment, false, isAReturn);
                }
                List<Product> productList = _ibl.GetAllProductsList();
                LoadProducts(productList);
                ListCart.Items.Clear();
            }
            else
            {
                MessageBox.Show("Pleas choose payment method");
            }
        }



        private void Payment(PaymentMethod payment, bool isCard, bool isAReturn)
        {

            Dictionary<int, Transaction.SoldProducts> receipt = GetReciept(isAReturn);
            if (receipt.Count > 0)
            {
                Transaction tran = _ibl.AddnewTransaction(DateTime.Now, isAReturn, payment, receipt);
                ClubMember clubMember = _ibl.GetClubMemberByUser(_user);
                if (clubMember != null && isCard)
                {
                    _ibl.AddNewTransactionId(clubMember.memberID, tran.TransactionID);
                    MessageBox.Show("Your card's information saved successfully and you paid successfully!");
                }
                else
                {
                    if (_user.Type == User.userType.Admin)
                    {
                        MessageBox.Show("Admin you have successfully added a transaction!");
                    }
                    else
                    {
                        MessageBox.Show("paid successfully!");
                    }

                }
            }
            else
            {
                MessageBox.Show("Please choose products");
            }
        }



        private void btnAdminPurchase_Click(object sender, RoutedEventArgs e)
        {
            Pay();
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {

            if (_ibl.FindUserType(_user.UserName, _user.Password).Equals(User.userType.Admin))
            {
                adminMain adminMain = new adminMain(_ibl, _user);
                this.Hide();
                adminMain.Show();
            }
            if (_ibl.FindUserType(_user.UserName, _user.Password).Equals(User.userType.Manager))
            {
                ManagerMenu ManMain = new ManagerMenu(_ibl, _user);
                this.Hide();
                ManMain.Show();
            }
            if (_ibl.FindUserType(_user.UserName, _user.Password).Equals(User.userType.Worker))
            {
                WorkerMenu WorkMain = new WorkerMenu(_ibl, _user);
                this.Hide();
                WorkMain.Show();
            }
            if ((_ibl.FindUserType(_user.UserName, _user.Password).Equals(User.userType.Customer) || (_ibl.FindUserType(_user.UserName, _user.Password).Equals(User.userType.ClubMember))))
            {
                MainWindow P = new MainWindow(_ibl);
                this.Hide();
                P.Show();
            }
        }

        private void txtMonth_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            txtMonth.Text = "";
        }

        private void txtYear_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            txtYear.Text = "";
        }

        private void btnReturn_Click(object sender, RoutedEventArgs e)
        {
            Pay(true);
        }

        private void btnStores_Click(object sender, RoutedEventArgs e)
        {
            Stores S = new Stores(_ibl, _user);
            this.Hide();
            S.Show();        
        }

    

    }

}


