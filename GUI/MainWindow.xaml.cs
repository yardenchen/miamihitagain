﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BL;
using BL_backend;
using GUI;
using DAL;





namespace GUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        
        IBL itsBL;


        


        public MainWindow(IBL itsBL)
        {
         //   this.Background = new ImageBrush(new BitmapImage(new Uri(@"Pictures\Background.jpg")));
            InitializeComponent();
            this.itsBL = itsBL;
            Show();
           

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            SignUp NewSign = new SignUp(itsBL);
            this.Hide();
            NewSign.Show();
        }

        private void userName_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            if (userNameField.Text.Equals("")  || PassField.Password.Equals("") )
            {
                MessageBox.Show("Invalid UserName or Password");
                return;
            }

            string userName = userNameField.Text;
            int pass = Convert.ToInt32(PassField.Password);
            BL_backend.User U = new BL_backend.User(userName, pass);
            if (itsBL.FindUser(userName, pass))
            {
                if (itsBL.FindUserType(userName, pass).Equals(BL_backend.User.userType.Admin))
                {
                    U.Type = User.userType.Admin;
                    adminMain adminMain = new adminMain(itsBL, U);
                    this.Hide();
                    adminMain.Show();
                }
                if (itsBL.FindUserType(userName, pass).Equals(BL_backend.User.userType.Manager))
                {
                    U.Type = User.userType.Manager;
                    ManagerMenu ManMain = new ManagerMenu(itsBL, U);
                    this.Hide();
                    ManMain.Show();
                }
                if (itsBL.FindUserType(userName, pass).Equals(BL_backend.User.userType.Worker))
                {
                    U.Type = User.userType.Worker;
                    WorkerMenu WorkMain = new WorkerMenu(itsBL, U);
                    this.Hide();
                    WorkMain.Show();
                }
              
                if ((itsBL.FindUserType(userName, pass).Equals(BL_backend.User.userType.Customer)))
                {
                    U.Type = User.userType.Customer;
                    PurchaseWindow P =  new PurchaseWindow(itsBL, U);
                    this.Hide();
                    P.Show();
                }

                if ((itsBL.FindUserType(userName, pass).Equals(BL_backend.User.userType.ClubMember)))
                {
                    U.Type = User.userType.ClubMember;
                    PurchaseWindow P = new PurchaseWindow(itsBL, U);
                    this.Hide();
                    P.Show();
                }

            }
            else
            {
                MessageBox.Show("Invalid UserName or Password");
            }


        }

       
       
    
    }

}