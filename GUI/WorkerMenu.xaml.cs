﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BL;
using BL_backend;


namespace GUI
{
    /// <summary>
    /// Interaction logic for WorkerMenu.xaml
    /// </summary>
    public partial class WorkerMenu : Window
    {
        private IBL itsBL;
        private User U;
        public WorkerMenu(IBL IBL, User U)
        {
            this.itsBL = IBL;
            this.U = U;
            InitializeComponent();
        }

        private void btnLogOut_Click(object sender, RoutedEventArgs e)
        {
            MainWindow main = new MainWindow(itsBL);
            main.Show();
            this.Close();
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            WorkerViewCM viewCM = new WorkerViewCM(itsBL, U);
            viewCM.Show();
            this.Close();
        }

        private void btnEditRemove_Click(object sender, RoutedEventArgs e)
        {
            WorkerEditPass edit = new WorkerEditPass(itsBL, U);
            edit.Show();
            this.Close();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            WorkerViewT viewT = new WorkerViewT(itsBL, U);
            viewT.Show();
            this.Close();
        }

        private void btnCart_Click(object sender, RoutedEventArgs e)
        {
            PurchaseWindow P = new PurchaseWindow(itsBL, U);
            P.Show();
            this.Close();
        }
    }
}
