﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BL;
using BL_backend;

namespace GUI
{
    /// <summary>
    /// Interaction logic for ManagerAddEmp.xaml
    /// </summary>
    public partial class ManagerAddEmp : Window
    {
        private IBL itsBL;
        private User U;
        public ManagerAddEmp(IBL IBL, User U)
        {
            this.itsBL = IBL;
            this.U = U;
            InitializeComponent();
        }

        private void btnCreate_Click(object sender, RoutedEventArgs e)
        {
            string FName = firstName.Text;
            string LName = lastName.Text;
            int Id = Int32.Parse(ID.Text);
            int deppId = Int32.Parse(departmentId.Text);
            int sal = Int32.Parse(salary.Text);
            int manId = Int32.Parse(manegerId.Text);
            string gender = cbGender.Text;
            try
            {
                itsBL.AddNewEmployee(sal, deppId, manId, Id, FName, LName, gender);
            }
            catch
            {
                MessageBox.Show("Invalid Input!");
                return;
            }

          MessageBox.Show("The Employee added succfully");  
        }

        private void btnGoBack_Click(object sender, RoutedEventArgs e)
        {
            ManagerMenu MM = new ManagerMenu(itsBL, U);
            MM.Show();
            this.Close();
        }
    }
}
