﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BL;
using BL_backend;

namespace GUI
{
    /// <summary>
    /// Interaction logic for quries.xaml
    /// </summary>
    public partial class quries : Window
    {
        private IBL itsBL;
        private User U;
        public quries(IBL IBL, User U)
        {
            this.itsBL = IBL;
            this.U = U;
            InitializeComponent();
            dgUser.ItemsSource = itsBL.GetAllUsers();
            dgClubMember.ItemsSource = itsBL.GetAllClubMembers();
            dgEmployee.ItemsSource = itsBL.GetAllEmployees();
            dgTransaction.ItemsSource = itsBL.GetAllTransactions();
            dgProduct.ItemsSource = itsBL.GetAllProductsList();
            
        }

       

        private void btnFilterUser_Click(object sender, RoutedEventArgs e)
        {

        
            {
                List<User> U = new List<User>();
                U = itsBL.FindUserByType((User.userType)Enum.Parse(typeof(User.userType), cbUserType.Text));
                dgUser.ItemsSource = U;
            }
            
          
        }

        private void dgUser_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void btnFilterP_Click(object sender, RoutedEventArgs e)
        {
            List<Product> P = new List<Product>();
            if (cbTypeP.Text.Equals("All"))
            {
                dgProduct.ItemsSource = itsBL.GetAllProductsList();
            }

            else
            {
                P = itsBL.GetProductListByType((productTypes)Enum.Parse(typeof(productTypes), cbTypeP.Text));
                dgProduct.ItemsSource = P;
            }
           

           
        }

        private void btnFilterPrice_Click(object sender, RoutedEventArgs e)
        {

            List<Product> P = new List<Product>();
            try
            {
                double price1 = Convert.ToDouble(HPrice.Text);
                double price2 = Convert.ToDouble(LPrice.Text);
                P = itsBL.GetProductListByPrice(price2, price1);
            }
            catch (Exception)
            {
                MessageBox.Show("Invalid Input");
            }
            dgProduct.ItemsSource = P;
        }
        private void dgProduct_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void btnFilterSalary_Click(object sender, RoutedEventArgs e)
        {
            List<Employee> E = new List<Employee>();
            try
            {
                double salary2 = Convert.ToDouble(HSalary.Text);
                double salary1 = Convert.ToDouble(LSalary.Text);
                E=itsBL.GetAllEmployeesBySalaryRange(salary1, salary2);
            }
            catch (Exception)
            {
                MessageBox.Show("Invalid Input");
            }

            dgEmployee.ItemsSource = E;
        }

        private void btnFilterDep_Click(object sender, RoutedEventArgs e)
        {
            List<Employee> E = new List<Employee>();
            try
            {
                int DipID = Int32.Parse(depID.Text);
                E=itsBL.GetAllEmployeeListByDepID(DipID);
            }
            catch (Exception)
            {
                MessageBox.Show("Invalid Input");
            }
            dgEmployee.ItemsSource = E;

        }

        private void dgEmployee_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void btnFilterCM_Click(object sender, RoutedEventArgs e)
        {
            List<ClubMember> C = new List<ClubMember>();
            try
            {
                int MemberID = Int32.Parse(memberID.Text);
                C=itsBL.GetAllClubMemberByClubID(MemberID);
            }
            catch (Exception)
            {
                MessageBox.Show("Invalid Input");
            }
            dgClubMember.ItemsSource = C;
        }

        private void btnFilterDateC_Click(object sender, RoutedEventArgs e)
        {
            List<ClubMember> C = new List<ClubMember>();
            try
            {
                DateTime date1 = (DateTime)dpC1.SelectedDate;
                DateTime date2 = (DateTime)dpC2.SelectedDate;
               C= itsBL.GetClubMemberListByDateOfBirth(date1, date2);
            }
            catch (Exception )
            {
                MessageBox.Show("Invalid Input");
            }
          
        
            dgClubMember.ItemsSource = C;
        }

        private void btnFilterT_Click(object sender, RoutedEventArgs e)
        {
            List<Transaction> T = new List<Transaction>();
            try
            {
                DateTime date1 = (DateTime)dpT.SelectedDate;
                DateTime Date2 = (DateTime)date2.SelectedDate;

               T= itsBL.GetAllTransactionListByDate(date1,Date2);
            }
            catch (Exception)
            {
                MessageBox.Show("Invalid Input");
            }


            dgTransaction.ItemsSource = T;
        }

        private void btnFilterPayment_Click(object sender, RoutedEventArgs e)
        {
            List<Transaction> T = new List<Transaction>();
           T= itsBL.GetTransactionListByPaymentMethod((PaymentMethod)Enum.Parse(typeof(PaymentMethod), cbPayment.Text));

            dgTransaction.ItemsSource = T;
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            adminMain main = new adminMain(itsBL, U);
            main.Show();
            this.Close(); 
        }

        private void cbTypeP_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void FilterByNamee_Click(object sender, RoutedEventArgs e)
        {
            List<Product> p = new List<Product>();

            if (itsBL.GetProductByName(NameField.Text) != null)
            {

                p.Add(itsBL.GetProductByName(NameField.Text));
                dgProduct.ItemsSource = p;
            }

            else
            {
                MessageBox.Show("Product not Found!");
            }
                 
        
        }

        private void NameField_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

   
       

       

       
      

        
    }
}
