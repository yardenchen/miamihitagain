﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BL;
using BL_backend;


namespace GUI
{
    /// <summary>
    /// Interaction logic for ManagerMenu.xaml
    /// </summary>
    public partial class ManagerMenu : Window
    {
        private IBL itsBL;
        private User U;
        public ManagerMenu(IBL IBL,User U)
        {
            this.itsBL = IBL;
            this.U = U;
            InitializeComponent();
        }


        private void btnLogOut_Click(object sender, RoutedEventArgs e)
        {
            MainWindow main = new MainWindow(itsBL);
            main.Show();
            this.Close();
        }
         
         

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            ManagerAddEmp addEmp = new ManagerAddEmp(itsBL, U);
            addEmp.Show();
            this.Close();
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            quruiesMan QM = new quruiesMan(itsBL, U);
            QM.Show();
            this.Close();
        }

        private void btnEditRemove_Click(object sender, RoutedEventArgs e)
        {
            Edit_Entities_Manager editman = new Edit_Entities_Manager(itsBL,this,U);
            editman.Show();
            this.Hide();

        }

        private void btnCart_Click(object sender, RoutedEventArgs e)
        {
            PurchaseWindow P = new PurchaseWindow(itsBL, U);
            P.Show();
            this.Close();
        }
    }
}
