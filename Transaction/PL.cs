﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BL_backend;
using BL;

namespace PL
{
    public class PL_CLI : IPL
    {
        //The Command Line Interface Implimentation of the Presentation Layer 

        private IBL itsBL;

        public PL_CLI(IBL bl)// note that the constructor expects an IBL type (and not a specific implementaion specific)
        {
            itsBL = bl;
        }

        private void DisplayResultP(List<Product> productList)
        {
            if (productList == null)
            {
                Console.WriteLine("There is no one that answer the Criteria ." + "\n");
                return;
            }
            foreach (Product P in productList)
            {
                Console.WriteLine(P.ToString());
                Console.WriteLine();
            }
            Console.WriteLine();
            Console.ReadLine();
        }

        private void DisplayResult(List<ClubMember> clubMemberList)
        {
            if (clubMemberList == null)
            {
                Console.WriteLine("There is no one that answer the Criteria ." + "\n");
                return;
            }
            foreach (ClubMember C in clubMemberList)
            {

                Console.WriteLine(C.ToString());
                Console.WriteLine();
            }
            Console.WriteLine();
            Console.ReadLine();
        }

        private void DisplayResult(List<Department> departmentList)
        {
            if (departmentList == null)
            {
                Console.WriteLine("There is no one that answer the Criteria ." + "\n");
                return;
            }
            foreach (Department D in departmentList)
            {
                Console.WriteLine(D.ToString());
                Console.WriteLine();
            }
            Console.WriteLine();
            Console.ReadLine();
        }



        private void DisplayResult(List<Employee> employeeList)
        {
            if (employeeList == null)
            {
                Console.WriteLine("There is no one that answer the Criteria ." + "\n");
                return;
            }
            foreach (Employee E in employeeList)
            {
                Console.WriteLine(E.ToString());
                Console.WriteLine();
            }
            Console.WriteLine();
            Console.ReadLine();
        }

        private void DisplayResult(List<Transaction> transactionList)
        {
            if (transactionList == null)
            {
                Console.WriteLine("There is no one that answer the Criteria ." + "\n");
                return;
            }
            foreach (Transaction T in transactionList)
            {
                Console.WriteLine(T.ToString());
                Console.WriteLine();
            }
            Console.WriteLine();
            Console.ReadLine();
        }



        public void Run()
        {
            bool dar = isUser();
            int count = 2;
            while ((!dar) && (count > 0))
            {
                Console.WriteLine("wrong code. try again. pay attention that you have more " + (count) + " attemps");
                dar = isUser();
                if (dar==false)
                count--;

            }
            if (count == 0)
            {
                Console.WriteLine("sorry, 3 times of wrong code. try again later. good bye");
                Console.ReadLine();
                System.Environment.Exit(0);//exit the program.
            }
          
            bool dar1 = FirstQuestion();
            while (!dar1)
            {
                dar1 = FirstQuestion();
            }
        }

        private static string ReadPassword() //Makes password invisible
        {
            ConsoleKeyInfo key;
            string pass = "";
            do
            {
                key = Console.ReadKey(true);

                // Backspace Should Not Work
                if (key.Key != ConsoleKey.Backspace && key.Key != ConsoleKey.Enter)
                {
                    pass += key.KeyChar;
                    Console.Write("*");
                }
                else
                {
                    if (key.Key == ConsoleKey.Backspace && pass.Length > 0)
                    {
                        pass = pass.Substring(0, (pass.Length - 1));
                        Console.Write("\b \b");
                    }
                }
            } while (key.Key != ConsoleKey.Enter);
            // Stops Receving Keys Once Enter is Pressed
          
            Console.WriteLine();
            return pass;
        }


        public bool isUser()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Welcome to the System.");
            Console.WriteLine("Please log in: ");
            Console.WriteLine("Enter your User Name (pay attention. It's key sensitive):");
            string userName = Console.ReadLine();
            Console.WriteLine("Enter you password");
            int pass;
            while (!int.TryParse(ReadPassword(), out pass))
            {

                Console.WriteLine("Invalid password. Try again (only numbers)");
            }

            return itsBL.FindUser(userName, pass);
        }


        public bool check(string newPass)
        {
            int len = newPass.Length;
            for (int i = 0; i < len; ++i)
            {
                char c = newPass[i];
                if (c < '0' || c > '9')
                    return false;
            }
            return true;
        }

        
        private string ReceiveCmd(int i)
        {
            string st = Console.ReadLine();
            if (i == 1)//FirstQuestion &&  GetSetPatientDetails
            {
                while (st != "1" && st != "2" && st != "3" && st != "0" && st != "4" && st != "5" && st != "6")
                {
                    Console.WriteLine("you entered an invalid number or string ,please press enter and try again ");
                    st = Console.ReadLine();
                }
                return st;
            }
            if (i == 2)//ProductQuestion
            {
                while (st != "1" && st != "2" && st != "3" && st != "0" && st != "4" && st != "5" && st != "6" && st != "7")
                {
                    Console.WriteLine("you entered an invalid number or string ,please press enter and try again ");
                    st = Console.ReadLine();
                }
                return st;
            }
            if (i == 3)//TransQuestion
            {
                while (st != "1" && st != "2" && st != "3" && st != "0" && st != "4" && st != "5" && st != "6" && st != "7")
                {
                    Console.WriteLine("you entered an invalid number or string ,please press enter and try again ");
                    st = Console.ReadLine();
                }
                return st;
            }
            if (i == 4)//CMQuestion
            {
                while (st != "1" && st != "2" && st != "3" && st != "0" && st != "4" && st != "5" && st != "6" && st != "7" && st != "8")
                {
                    Console.WriteLine("you entered an invalid number or string ,please press enter and try again ");
                    st = Console.ReadLine();
                }
                return st;
            }
            if (i == 5)//EmpQuestion
            {
                while (st != "1" && st != "2" && st != "3" && st != "0" && st != "4" && st != "5" && st != "6" && st != "7" && st != "8")
                {
                    Console.WriteLine("you entered an invalid number or string ,please press enter and try again ");
                    st = Console.ReadLine();
                }
                return st;
            }
            if (i == 6)//DepQuestion
            {
                while (st != "1" && st != "2" && st != "3" && st != "0" && st != "4" && st != "5" && st != "6")
                {
                    Console.WriteLine("you entered an invalid number or string ,please press enter and try again ");
                    st = Console.ReadLine();
                }
                return st;
            }
            if (i == 10)//id
            {

                bool flag = false;
                while (!flag)
                {
                    try
                    {
                        int id = Convert.ToInt32(st);
                        flag = st.Length == 9;
                        if (!flag)
                        {
                            Console.WriteLine("you entered an invalid number or string ,please press enter and try again ");
                            st = Console.ReadLine();
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("you entered an invalid number or string ,please press enter and try again ");
                        st = Console.ReadLine();
                    }
                }
                return st;
            }
            if (i == 11)//name
            {
                return st;
            }
            if (i == 12)//gender
            {
                while (st != "male" && st != "Male" && st != "female" && st != "Female")
                {
                    Console.WriteLine("you entered an invalid number or string ,please press enter and try again ");
                    st = Console.ReadLine();
                }
                if (st == "male" || st == "Male")
                    return "Male";
                else
                    return "Female";

            }
            if (i == 13)//salary
            {

                bool flag = false;
                while (!flag)
                {
                    try
                    {
                        double sal = Convert.ToDouble(st);
                        flag = sal > 0;
                        if (!flag)
                        {
                            Console.WriteLine("you entered an invalid number or string ,please press enter and try again ");
                            st = Console.ReadLine();
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("you entered an invalid number or string ,please press enter and try again ");
                        st = Console.ReadLine();
                    }
                }
                return st;
            }
            if (i == 14)// day
            {
                bool flag = false;
                while (!flag)
                {
                    try
                    {
                        int day = Convert.ToInt32(st);
                        flag = day > 0 && day < 32;
                        if (!flag)
                        {
                            Console.WriteLine("you entered an invalid number or string ,please press enter and try again ");
                            st = Console.ReadLine();
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("you entered an invalid number or string ,please press enter and try again ");
                        st = Console.ReadLine();
                    }
                }
                return st;

            }
            if (i == 15)// month
            {
                bool flag = false;
                while (!flag)
                {
                    try
                    {
                        int month = Convert.ToInt32(st);
                        flag = month > 0 && month < 13;
                        if (!flag)
                        {
                            Console.WriteLine("you entered an invalid number or string ,please press enter and try again ");
                            st = Console.ReadLine();
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("you entered an invalid number or string ,please press enter and try again ");
                        st = Console.ReadLine();
                    }
                }
                return st;
            }
            if (i == 16)// year
            {
                bool flag = false;
                while (!flag)
                {
                    try
                    {
                        int year = Convert.ToInt32(st);
                        flag = year > 1000 && year < 9999;
                        if (!flag)
                        {
                            Console.WriteLine("you entered an invalid number or string ,please press enter and try again ");
                            st = Console.ReadLine();
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("you entered an invalid number or string ,please press enter and try again ");
                        st = Console.ReadLine();
                    }
                }
                return st;

            }
            if (i == 17)//ans
            {
                while (st != "no" && st != "No" && st != "Yes" && st != "yes")
                {
                    Console.WriteLine("you entered an invalid number or string ,please press enter and try again ");
                    st = Console.ReadLine();
                }
                if (st == "no" || st == "No")
                    return "No";
                else
                    return "Yes";

            }
            if (i == 18)//ans
            {
                while (st != "1" && st != "2")
                {
                    Console.WriteLine("you entered an invalid number or string ,please press enter and try again ");
                    st = Console.ReadLine();
                }
                return st;
            }
            if (i == 19)//edit
            {
                while (st != "1" && st != "2" )
                {
                    Console.WriteLine("you entered an invalid number or string ,please press enter and try again ");
                    st = Console.ReadLine();
                }
                return st;
            }
            if (i == 20)//id
            {

                bool flag = false;
                while (!flag)
                {
                    try
                    {
                        int id = Convert.ToInt32(st);
                        flag = st.Length == 9 || id == -1;
                        if (!flag)
                        {
                            Console.WriteLine("you entered an invalid number or string ,please press enter and try again ");
                            st = Console.ReadLine();
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("you entered an invalid number or string ,please press enter and try again ");
                        st = Console.ReadLine();
                    }
                }
                return st;
            }


            return st;
        }

        public string reciveCMD(string type)
        {
            if (type == string.Empty)
            {
                type = Console.ReadLine();
            }
            while (!(productTypes.Cleaning.ToString() == type) && !(productTypes.Electronics.ToString() == type) && !(productTypes.Furniture.ToString() == type) && !(productTypes.None.ToString() == type))
            {
                Console.WriteLine("Invalid Product type. please try again : ");
                type = Console.ReadLine();
            }
            return type;
        }

        public string reciveCMD2(string type)
        {
            while (!(PaymentMethod.AmericanExpress.ToString() == type) && !(PaymentMethod.Cash.ToString() == type) && !(PaymentMethod.Check.ToString() == type) && !(PaymentMethod.Isracard.ToString() == type) && !(PaymentMethod.MasterCard.ToString() == type) && !(PaymentMethod.Other.ToString() == type) && !(PaymentMethod.Visa.ToString() == type))
            {
                Console.WriteLine("Invalid payment method. please try again : ");
                type = Console.ReadLine();
            }
            return type;
        }



private bool FirstQuestion()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.Clear();
            Console.WriteLine("Hello and welcome to E-mart Department Store", "\n");
            Console.WriteLine("1 = Products in the store", "\n");
            Console.WriteLine("2 = Transactions", "\n");
            Console.WriteLine("3 = Club Member", "\n");
            Console.WriteLine("4 = Employee", "\n");
            Console.WriteLine("5 = Departments", "\n");
            Console.WriteLine("6 = Users");
            Console.WriteLine("0 = Exit the system", "\n");
            Console.WriteLine("Please answer", "\n");
            return FirstQuestionAns(ReceiveCmd(1));
        }

private bool FirstQuestionAns(string firstQuestion)
{

    if (firstQuestion == "1")
    {
        ProductQuestion();
        return true;
    }

    if (firstQuestion == "2")
    {
        TransactionQuestion();
        return true;
    }

    if (firstQuestion == "3")
    {
        ClubMemberQuestion();
        return true;

    }

    if (firstQuestion == "4")
    {
        EmployeeQuestion();
        return true;

    }

    if (firstQuestion == "5")
    {
        DepartmentQuestion();
        return true;

    }
    if (firstQuestion == "6")
    {
        UserQuestion();
        return true;


    }
        if (firstQuestion == "0")
        {
            Console.Clear();
            Console.WriteLine("Thank you for using E-mart Department Store! :)");
            Console.WriteLine("press enter to exit the system");
            Console.ReadLine();
            Environment.Exit(0);
            return false;
        }
        return true;

    }


        private void ProductQuestion()
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Clear();
            Console.WriteLine("Hello manager ", "\n");
            Console.WriteLine("to go back to the main menu press 0", "\n");
            Console.WriteLine("1 = add a new product", "\n");
            Console.WriteLine("2 = remove existing product", "\n");
            Console.WriteLine("3 = get data and update new data on existing product", "\n");
            Console.WriteLine("4 = get list of products by thier type", "\n");
            Console.WriteLine("5 = get list of products by thier price", "\n");
            Console.WriteLine("6 = get list of all the products", "\n");
            Console.WriteLine("7 = find a product by his InventoryID", "\n");
            Console.WriteLine("please answer:", "\n");
            ProductQuestionAns(ReceiveCmd(2));

        }

        private void ProductQuestionAns(string preQuestion)
        {
            if (preQuestion == "1")
            {
                string ans = "1";
                while (ans.Equals("1"))
                {
                    Console.Clear();
                    Console.WriteLine("please enter the product 9 digit inventory ID number");
                    string invid = ReceiveCmd(10);
                    int Invid = Convert.ToInt32(invid);
                    while (itsBL.FindProduct(Invid))
                    {
                        Console.WriteLine("the product's ID you entered is already exist in the system. Let's try again : ");
                        invid = ReceiveCmd(10);
                        Invid = Convert.ToInt32(invid);
                    }
                    Console.WriteLine("please enter the new product's name");
                    string name = ReceiveCmd(11);
                    Console.WriteLine("please enter the new product's type: Electronics \\ Furniture\\ Cleaning\\ None ");
                    string type = reciveCMD(string.Empty);
                    productTypes Type = (productTypes)Enum.Parse(typeof(productTypes), type);
                    Console.WriteLine("please enter the new product's location (9 didgit number )");
                    string location = ReceiveCmd(11);
                    int Location = Int32.Parse(location);
                    Console.WriteLine("please enter the new product's price");
                    string price = ReceiveCmd(13);
                    double Price = Convert.ToDouble(price);
                    var newStockValue = itsBL.GetStockCountOfProduct(Invid) + 1;
                    itsBL.AddnewProduct(name, Type, Invid, Location, true, newStockValue, Price);
                    Console.WriteLine("The product added succuffuly . ");
                    Console.WriteLine("Would you like to add another product ?  Press: ");
                    Console.WriteLine("(1) Yes ", "\n"); Console.WriteLine("(2) No, go back please ", "\n");
                    ans = ReceiveCmd(18);

                }
                ProductQuestion();
            }

            if (preQuestion == "2")
            {
                string ans = "1";
                while (ans.Equals("1"))
                {
                    Console.Clear();
                    Console.WriteLine("please enter the product 9 digit inventory ID number");
                    string invid = ReceiveCmd(10);
                    int Invid = Convert.ToInt32(invid);
                    while (!itsBL.FindProduct(Invid))
                    {
                        Console.WriteLine("the Product's ID you entered is not recognized by the system,\n please enter the correct ID");
                        invid = ReceiveCmd(10);
                        Invid = Convert.ToInt32(invid);
                    }

                    itsBL.deleteProduct(Invid);
                    Console.WriteLine("The Product removed succsfully");
                    Console.WriteLine("Would you like to remove another product ?  Press: ");
                    Console.WriteLine("(1) Yes ", "\n"); Console.WriteLine("(2) No, go back please ", "\n");
                    ans = ReceiveCmd(18);

                }
                ProductQuestion();
            }

            if (preQuestion == "3")
            {
                string ans = "1";
                while (ans.Equals("1"))
                {
                    Console.Clear();
                    Console.WriteLine("please enter the product 9 digit inventory ID number");
                    string invid = ReceiveCmd(10);
                    int Invid = Convert.ToInt32(invid);
                    while (!itsBL.FindProduct(Invid))
                    {
                        Console.WriteLine("the Product's ID you entered is not recognized by the system, please enter the correct ID");
                        invid = ReceiveCmd(10);
                        Invid = Convert.ToInt32(invid);
                    }
                    Console.WriteLine("please enter the Count of the product you want to update to");
                    string stockProduct = Console.ReadLine();
                    int stockProd = Convert.ToInt32(stockProduct);
                    while (stockProd < 0)
                    {
                        Console.WriteLine("the Product's Count you entered is not acceptable by the system, please enter the correct count of stock");
                        stockProduct = ReceiveCmd(4);
                        stockProd = Convert.ToInt32(stockProduct);
                    }

                    //itsBL.updateProduct(Invid, stockProd);
                    Console.WriteLine("Done. Would you like to edit another product? Press:  ");
                    Console.WriteLine("(1) Yes ", "\n"); Console.WriteLine("(2) No, go back please ", "\n");
                    ans = ReceiveCmd(18);
                }
                ProductQuestion();

            }

            if (preQuestion == "4")
            {
                string ans = "1";
                while (ans.Equals("1"))
                {
                    Console.Clear();
                    Console.WriteLine("please enter the new product's type: Electronics \\ Furniture\\ Cleaning\\ None");
                    string type = reciveCMD(string.Empty);
                    productTypes Type = (productTypes)Enum.Parse(typeof(productTypes), type);
                    DisplayResultP(itsBL.GetProductListByType(Type));
                    Console.WriteLine("Done. Would you like to get list of another type? Press:  ");
                    Console.WriteLine("(1) Yes ", "\n"); Console.WriteLine("(2) No, go back please ", "\n");
                    ans = ReceiveCmd(18);
                }
                ProductQuestion();

            }

            if (preQuestion == "5")
            {
                string ans = "1";
                while (ans.Equals("1"))
                {
                    Console.Clear();
                    Console.WriteLine("please enter the product's minimal price");
                    string min = ReceiveCmd(13);
                    double Min = Convert.ToDouble(min);
                    Console.WriteLine("please enter the product's maxmimal price");
                    string max = ReceiveCmd(13);
                    double Max = Convert.ToDouble(max);
                    while (Min > Max)
                    {
                        Console.WriteLine("Sorry. Incorrect input. Let's try again: ");
                        Console.WriteLine("Please enter the minimum price ");
                        min = ReceiveCmd(13);
                        Min = Convert.ToDouble(min);
                        Console.WriteLine("Please enter the maximum price ");
                        max = ReceiveCmd(13);
                        Max = Convert.ToDouble(max);
                    }
                    DisplayResultP(itsBL.GetProductListByPrice(Min, Max));
                    Console.WriteLine("Done. Would you like to get list with another price range? Press:  ");
                    Console.WriteLine("(1) Yes ", "\n"); Console.WriteLine("(2) No, go back please ", "\n");
                    ans = ReceiveCmd(18);

                }
                ProductQuestion();

            }

            if (preQuestion == "6")
            {
                Console.Clear();
                DisplayResultP(itsBL.GetAllProductsList());
                Console.ReadLine();

                ProductQuestion();

            }



            if (preQuestion == "7")
            {
                string ans = "1";
                while (ans.Equals("1"))
                {
                    Console.Clear();
                    Console.WriteLine("please enter the product's 9 digit InventoryID number");
                    string prodID = ReceiveCmd(10);
                    int id = Convert.ToInt32(prodID);
                    while (!itsBL.FindProduct(id))
                    {
                        Console.WriteLine("the product's ID you entered is not recognized by the system,\n please enter the correct ID");
                        prodID = ReceiveCmd(10);
                        id = Convert.ToInt32(prodID);
                    }

                    Console.WriteLine("Done. Would you like to get another product by hid ID? Press:  ");
                    Console.WriteLine("(1) Yes ", "\n"); Console.WriteLine("(2) No, go back please ", "\n");
                    ans = ReceiveCmd(18);

                }
                ProductQuestion();

            }

            if (preQuestion == "0")
            {
                FirstQuestion();
                
            }
        }

        private void TransactionQuestion()
        {
            Console.Clear();
            Console.WriteLine("Hello manager ", "\n");
            Console.WriteLine("to go back to the main menu press 0", "\n");
            Console.WriteLine("1 = Check if Transaction exist By ID", "\n");
            Console.WriteLine("2 = add new Transaction", "\n");
            Console.WriteLine("3 = delete Transaction", "\n");
            Console.WriteLine("4 = get all Transactions by thier date of issue", "\n");
            Console.WriteLine("5 = get all the Transactions by their Payment Methods", "\n");
            Console.WriteLine("6 = get all the Transactions", "\n");
            Console.WriteLine("7 = edit exiting Transactions", "\n");
            Console.WriteLine("Please answer", "\n");
            TransactionQuestionAns(ReceiveCmd(3));
        }

        private void TransactionQuestionAns(string tranQuestion)
        {
            if (tranQuestion == "1")
            {

                string ans = "1";
                while (ans.Equals("1"))
                {
                    Console.Clear();
                    Console.WriteLine("please enter the 9 digit Transaction ID number");
                    string id = Console.ReadLine();
                    while (id == null)
                    {
                        Console.WriteLine("Sorry, you didn't enter nothing. Please try again. please enter the 9 digit Transaction ID number");
                        id = Console.ReadLine();
                    }
                    bool exist = itsBL.FindTransactionByID(id);
                    if (exist)
                    {
                        Console.WriteLine("The Transaction is exist in the in the system ");
                    }
                    else
                        Console.WriteLine("Sorry, the Transaction is not exist in the in the system ");
                    Console.WriteLine("Done. Would you like to check for another product? Press:  ");
                    Console.WriteLine("(1) Yes ", "\n"); Console.WriteLine("(2) No, go back please ", "\n");
                    ans = ReceiveCmd(18);

                }
                TransactionQuestion();

            }


            if (tranQuestion == "2")
            {
                Console.Clear();
                Dictionary<int, BL_backend.Transaction.SoldProducts> recipte = new Dictionary<int, BL_backend.Transaction.SoldProducts>();
                

                Console.WriteLine("Are you a club member ? press:");
                Console.WriteLine("(1) Yes ", "\n"); Console.WriteLine("(2) No ", "\n");
                string answer = ReceiveCmd(18);
                if (answer.Equals("1"))
                {
                    string ans = "1";
                    while (ans.Equals("1"))
                    {
                        Console.WriteLine("Please enter the 9 digit club member SSN:");
                        string Id = ReceiveCmd(10);
                        int id = Convert.ToInt32(Id);
                        bool exist = itsBL.FindClubMemberByID(id);
                        while (!exist)
                        {
                            Console.WriteLine("Sorry, the system is not recognize you");
                            Console.WriteLine("Please enter the 9 digit club member ID:");
                             Id = ReceiveCmd(10);
                             id = Convert.ToInt32(Id);
                            exist = itsBL.FindClubMemberByID(id);
                            
                        }
                        bool buy = true;
                        while (buy)
                        {
                            Console.WriteLine("Please add the 9 digit ID of the product you want to purchase, ");
                            Console.WriteLine("if you'll write -1 you'll stop to purchse");
                            string invid = ReceiveCmd(20);
                          
                            int Invid = Convert.ToInt32(invid);
                            if (Invid == -1)
                            {
                                buy = false;
                            }

                            else
                            {
                                Product selectedProduct = itsBL.GetAllProductsList().First(x => x.InventoryID == Invid);
                                Console.Clear();
                                Console.WriteLine("you got " + selectedProduct.getSC() + " in stock .please enter the desired amount of products (" + selectedProduct.getName() + ") you want to buy:");

                                string qunatity = Console.ReadLine();
                                while (!check(qunatity) || qunatity == null)
                                {
                                    Console.WriteLine("The qunatity you entered does not contain ONLY digit. Let's try again : ");
                                    qunatity = Console.ReadLine();
                                }

                                int qunatityInt = Convert.ToInt32(qunatity);
                                while (qunatityInt > selectedProduct.getSC())
                                {
                                    Console.WriteLine("The amount you entered is not in stock.");
                                    qunatity = Console.ReadLine();
                                    qunatityInt = Convert.ToInt32(qunatity);
                                }
                                BL_backend.Transaction.SoldProducts temp2 = new BL_backend.Transaction.SoldProducts(selectedProduct, qunatityInt);
                                if (!recipte.ContainsKey(temp2.ID))
                                {
                                    recipte.Add(temp2.ID, temp2);

                                    Console.Clear();
                                }
                                else
                                {
                                    recipte[temp2.ID].setQua(recipte[temp2.ID].quantity + temp2.quantity);
                                    Console.Clear();
                                }
                            }
                            
                        }
                        DateTime now = DateTime.Now;
                        Console.WriteLine("Please enter the payment mathod you would like to pay with, write it manually : Visa, Cash, MasterCard, Isracard,  Other, AmericanExpress, Check");
       
                        string type = reciveCMD2(Console.ReadLine());
                        PaymentMethod paymentMethod;
                        Enum.TryParse("type", out paymentMethod);

                        itsBL.AddnewTransaction(now, false, paymentMethod, recipte);
                        List<Transaction> tempT = itsBL.GetAllTransactions();
                        itsBL.AddNewTransactionId(id,tempT[tempT.Count-1].getID());
                        


                        Console.WriteLine("Done. Would you like to add another Transaction? Press:  ");
                        Console.WriteLine("(1) Yes ", "\n"); Console.WriteLine("(2) No, go back please ", "\n");
                        ans = ReceiveCmd(18);
                    }
                    TransactionQuestion();
                }


                else
                {
                    string ans = "1";
                    while (ans.Equals("1"))
                    {
            
                        bool buy = true;
                        while (buy)
                        {
                            Console.WriteLine("Please add the 9 digit ID of the product you want to purchase, ");
                            Console.WriteLine("if you'll write -1 you'll stop to purchse");
                            string invid = ReceiveCmd(20);

                            int Invid = Convert.ToInt32(invid);
                            if (Invid == -1)
                            {
                                buy = false;
                            }

                            else
                            {
                                Product selectedProduct = itsBL.GetAllProductsList().First(x => x.InventoryID == Invid);
                                Console.Clear();
                                Console.WriteLine("you got " + selectedProduct.getSC() + " in stock .please enter the desired amount of products (" + selectedProduct.getName() + ") you want to buy:");

                                string qunatity = Console.ReadLine();
                                while (!check(qunatity) || qunatity == null)
                                {
                                    Console.WriteLine("The qunatity you entered does not contain ONLY digit. Let's try again : ");
                                    qunatity = Console.ReadLine();
                                }

                                int qunatityInt = Convert.ToInt32(qunatity);
                                while (qunatityInt > selectedProduct.getSC())
                                {
                                    Console.WriteLine("The amount you entered is not in stock.");
                                    qunatity = Console.ReadLine();
                                    qunatityInt = Convert.ToInt32(qunatity);
                                }
                                BL_backend.Transaction.SoldProducts temp2 = new BL_backend.Transaction.SoldProducts(selectedProduct, qunatityInt);
                                if (!recipte.ContainsKey(temp2.ID))
                                {
                                    recipte.Add(temp2.ID, temp2);

                                    Console.Clear();
                                }
                                else
                                {
                                    recipte[temp2.ID].setQua(recipte[temp2.ID].quantity + temp2.quantity);
                                    Console.Clear();
                                }
                            }

                        }
                        DateTime now = DateTime.Now;
                        Console.WriteLine("Please enter the payment mathod you would like to pay with, write it manually : Visa, Cash, MasterCard, Isracard,  Other, AmericanExpress, Check");

                        string type = reciveCMD2(Console.ReadLine());
                        PaymentMethod paymentMethod;
                        Enum.TryParse("type", out paymentMethod);

                        itsBL.AddnewTransaction(now, false, paymentMethod, recipte);
                       


                        Console.WriteLine("Done. Would you like to add another Transaction? Press:  ");
                        Console.WriteLine("(1) Yes ", "\n"); Console.WriteLine("(2) No, go back please ", "\n");
                        ans = ReceiveCmd(18);

                    }
                    TransactionQuestion();
                }

            }


            if (tranQuestion == "3")
            {
                string ans = "1";
                while (ans.Equals("1"))
                {
                    Console.Clear();
                    Console.WriteLine("please enter the 9 digit Transaction ID number", "\n");
                    string tranId = Console.ReadLine();
                    while (tranId == null)
                    {
                        Console.WriteLine("Sorry, you didn't enter nothing. Please try again. please enter the 9 digit Transaction ID number");
                        tranId = Console.ReadLine();
                        bool exist = itsBL.FindTransactionByID(tranId);
                        while (exist)
                        {
                            Console.WriteLine("Sorry, The Transaction you entered is not exist in the in the system, please try again", "\n");
                            tranId = Console.ReadLine();

                        }

                        itsBL.deleteTransaction(tranId);
                        Console.WriteLine("Done. Would you like to remove another Transaction? Press:  ");
                        Console.WriteLine("(1) Yes ", "\n"); Console.WriteLine("(2) No, go back please ", "\n");
                        ans = ReceiveCmd(18);

                    }
                    TransactionQuestion();
                }
            }

                if (tranQuestion == "4")
                {
                    string anss = "1";
                    while (anss.Equals("1"))
                    {
                        Console.Clear();
                        Console.WriteLine("please enter the transaction's date of issue. ", "\n");
                        Console.WriteLine("please enter the year");
                        string year = ReceiveCmd(16);
                        int Fyear = Convert.ToInt32(year);
                        Console.WriteLine("please enter the month");
                        string month = ReceiveCmd(15);
                        int Fmonth = Convert.ToInt32(month);
                        Console.WriteLine("please enter the day");
                        string day = ReceiveCmd(14);
                        int Fday = Convert.ToInt32(day);
                        string all = string.Concat(year, month, day);
                        DateTime Date = new DateTime(Fyear, Fmonth, Fday);
                        DisplayResult(itsBL.GetAllTransactionListByDate(Date));
                        Console.WriteLine("Done. Would you like to get another Transaction? Press:  ");
                        Console.WriteLine("(1) Yes ", "\n"); Console.WriteLine("(2) No, go back please ", "\n");
                        anss = ReceiveCmd(18);

                    }
                    TransactionQuestion();
                }
            
                if (tranQuestion == "5")
                {
                    string anss = "1";
                    while (anss.Equals("1"))
                    {
                        Console.Clear();
                        Console.WriteLine("please enter your the type of the payment method");
                        string type = reciveCMD2(string.Empty);
                        PaymentMethod paymentMethod = (PaymentMethod)Enum.Parse(typeof(PaymentMethod), type);
                        DisplayResult(itsBL.GetTransactionListByPaymentMethod(paymentMethod));
                        Console.WriteLine("Done. Would you like to get another Transaction by payment method? Press:  ");
                        Console.WriteLine("(1) Yes ", "\n"); Console.WriteLine("(2) No, go back please ", "\n");
                        anss = ReceiveCmd(18);

                    }
                    TransactionQuestion();
                }

                if (tranQuestion == "6")
                {
                    Console.Clear();
                    DisplayResult(itsBL.GetAllTransactions());
                    Console.ReadLine();

                    TransactionQuestion();
                }

                if (tranQuestion == "7")
                {

                    string ans = "1";
                    while (ans.Equals("1"))
                    {
                        Console.Clear();
                        Console.WriteLine("please enter the 9 digit Transaction ID number");
                        string id = Console.ReadLine();
                        while (id == null)
                        {
                            Console.WriteLine("Sorry, you didn't enter nothing. Please try again. please enter the 9 digit Transaction ID number");
                            id = Console.ReadLine();
                        }
                        bool exist = itsBL.FindTransactionByID(id);
                        if (exist)
                        {
                         
                            Console.WriteLine("Would you like to update the transaction from the status RETURN? ");
                            Console.WriteLine("Press (1) for yes (2) to go back to the main menu ");
                            string edit = ReceiveCmd(19);
                            int EDIT = Convert.ToInt32(edit);
                            if (EDIT == 1)
                            {
                                //itsBL.updateTransaction(id, true);
                            }
                            else
                            {
                                TransactionQuestion();
                            }

                        }
                        else
                        Console.WriteLine("Sorry, the Transaction is not exist in the in the system ");
                        Console.WriteLine("Done. Would you like to edit for another transaction? Press:  ");
                        Console.WriteLine("(1) Yes ", "\n"); Console.WriteLine("(2) No, go back please ", "\n");
                        ans = ReceiveCmd(18);

                    }
                    TransactionQuestion();

                }
                if (tranQuestion == "0")
                {

                    FirstQuestion();
                }
            }
        


        private void ClubMemberQuestion()
        {
            Console.Clear();
            Console.WriteLine("Hello manager ", "\n");
            Console.WriteLine("to go back to the main menu press 0", "\n");
            Console.WriteLine("1 = Check if a Club Member exist by his SSN ", "\n");
            Console.WriteLine("2 = Add a new Club Member", "\n");
            Console.WriteLine("3 = Remove a Club Member ", "\n");
            Console.WriteLine("4 = Get Club Members By their Date Of Birth range", "\n");
            Console.WriteLine("5 = Get a Club Member by his member ID number ", "\n");
            Console.WriteLine("6 = Get a list of all the Club Members ", "\n");
            Console.WriteLine("7 = Edit a Club Member details ", "\n");
            Console.WriteLine("Please answer", "\n");
            ClubMemberQuesionAns(ReceiveCmd(4));
        }

        private void ClubMemberQuesionAns(string CMQuestion)
        {
            if (CMQuestion == "1")
            {
                string anss = "1";
                while (anss.Equals("1"))
                {
                    Console.Clear();
                    Console.WriteLine("please enter the club member's SSN (9 digit)");
                    string Id = ReceiveCmd(10);
                    int id = Convert.ToInt32(Id);
                    bool exist = itsBL.FindClubMemberByID(id);
                    if (exist)
                        Console.WriteLine("The Club Member is exist in the in the system ");

                    else
                        Console.WriteLine("Sorry, the Club Member is not exist in the in the system ");
                    Console.WriteLine("Done. Would you like to get another club member by SSN? Press:  ");
                    Console.WriteLine("(1) Yes ", "\n"); Console.WriteLine("(2) No, go back please ", "\n");
                    anss = ReceiveCmd(18);

                }
                ClubMemberQuestion();
            }

            if (CMQuestion == "2")
            {
                string anss = "1";
                while (anss.Equals("1"))
                {
                    Console.Clear();
                    Console.WriteLine("please enter the 9 digit member ID");
                    string memberID = ReceiveCmd(10);
                    int MemberID = Convert.ToInt32(memberID);
                    while (itsBL.FindClubMemberByID(MemberID))
                    {
                        Console.WriteLine("the Club Member's ID you entered is already exist in the system. Let's try again : ");
                        memberID = ReceiveCmd(10);
                        MemberID = Convert.ToInt32(memberID);
                    }
                    Console.WriteLine("please enter the club member date of birth. ", "\n");
                    Console.WriteLine("please enter the club member year ", "\n");
                    string year = ReceiveCmd(16);
                    int Fyear = Convert.ToInt32(year);
                    Console.WriteLine("please enter the month");
                    string month = ReceiveCmd(15);
                    int Fmonth = Convert.ToInt32(month);
                    Console.WriteLine("please enter the day");
                    string day = ReceiveCmd(14);
                    int Fday = Convert.ToInt32(day);
                    string all = string.Concat(year+"/", month+"/", day);
                    DateTime Date = Convert.ToDateTime(all);

                    Console.WriteLine("please enter the member SSN");
                    string ssn = ReceiveCmd(10);
                    int SSN = Convert.ToInt32(ssn);
                    Console.WriteLine("please enter the club member first name");
                    string Fname = ReceiveCmd(11);
                    Console.WriteLine("please enter the club member last name");
                    string Lname = ReceiveCmd(11);
                    Console.WriteLine("please enter club member gender");
                    string gender = ReceiveCmd(12);
                    //itsBL.AddnewClubMember(MemberID, Date, SSN, Fname, Lname, gender);
                    Console.WriteLine("The club member added succussfuly");
                    Console.WriteLine("Done. Would you like to add another club member by SSN? Press:  ");
                    Console.WriteLine("(1) Yes ", "\n"); Console.WriteLine("(2) No, go back please ", "\n");
                    anss = ReceiveCmd(18);

                }
                ClubMemberQuestion();
            }

            if (CMQuestion == "3")
            {
                string anss = "1";
                while (anss.Equals("1"))
                {
                    Console.Clear();
                    Console.WriteLine("please enter the club member SSN");
                    string ssn = ReceiveCmd(10);
                    int SSN = Convert.ToInt32(ssn);
                    itsBL.deleteClubMember(SSN);
                    Console.WriteLine("The club member removed succussfuly");
                    Console.WriteLine("Done. Would you like to remove another club member by SSN? Press:  ");
                    Console.WriteLine("(1) Yes ", "\n"); Console.WriteLine("(2) No, go back please ", "\n");
                    anss = ReceiveCmd(18);

                }
                ClubMemberQuestion();
            }
            if (CMQuestion == "4")
            {
                string anss = "1";
                while (anss.Equals("1"))
                {
                    Console.Clear();
                    Console.WriteLine("please enter the 1st club member date of birth. ", "\n");
                    Console.WriteLine("please enter the club member year ", "\n");
                    string year = ReceiveCmd(16);
                    int Fyear = Convert.ToInt32(year);
                    Console.WriteLine("please enter the month");
                    string month = ReceiveCmd(15);
                    int Fmonth = Convert.ToInt32(month);
                    Console.WriteLine("please enter the day");
                    string day = ReceiveCmd(14);
                    int Fday = Convert.ToInt32(day);
                    string all = string.Concat(year, month, day);
                    DateTime Date = new DateTime(Fyear, Fmonth, Fday);
                    Console.WriteLine("Please enter the 2nd range club member date of birth");
                    Console.WriteLine("please enter the club member year ", "\n");
                    string year2 = ReceiveCmd(16);
                    int Fyear2 = Convert.ToInt32(year2);
                    Console.WriteLine("please enter the month");
                    string month2 = ReceiveCmd(15);
                    int Fmonth2 = Convert.ToInt32(month2);
                    Console.WriteLine("please enter the day");
                    string day2 = ReceiveCmd(14);
                    int Fday2 = Convert.ToInt32(day2);
                    string all2 = string.Concat(year, month, day);
                    DateTime Date2 = new DateTime(Fyear2, Fmonth2, Fday2);
                    DisplayResult(itsBL.GetClubMemberListByDateOfBirth(Date, Date2));
                    Console.WriteLine("Done. Would you like to find another club member by range of date? Press:  ");
                    Console.WriteLine("(1) Yes ", "\n"); Console.WriteLine("(2) No, go back please ", "\n");
                    anss = ReceiveCmd(18);

                }
                ClubMemberQuestion();
            }
            if (CMQuestion == "5")
            {
                string anss = "1";
                while (anss.Equals("1"))
                {
                    Console.Clear();
                    Console.WriteLine("please enter the club member 9 digit ID");
                    string id = ReceiveCmd(10);
                    int ID = Convert.ToInt32(id);
                    DisplayResult(itsBL.GetAllClubMemberByClubID(ID));
                    Console.WriteLine("Done. Would you like to find another club member by hid ID? Press:  ");
                    Console.WriteLine("(1) Yes ", "\n"); Console.WriteLine("(2) No, go back please ", "\n");
                    anss = ReceiveCmd(18);

                }
                ClubMemberQuestion();
            }
            if (CMQuestion == "6")
            {
                Console.Clear();
                DisplayResult(itsBL.GetAllClubMembers());
                Console.ReadLine();

                ClubMemberQuestion();
            }
             if (CMQuestion == "7")
            {
                string anss = "1";
                while (anss.Equals("1"))
                {
                    Console.Clear();
                    Console.WriteLine("please enter the club member's SSN (9 digit)");
                    string Id = ReceiveCmd(10);
                    int id = Convert.ToInt32(Id);
                    bool exist = itsBL.FindClubMemberByID(id);
                    if (exist)
                    {
                            Console.WriteLine("Would you like to update the club member first and last name? ");
                            Console.WriteLine("Press (1) for yes (2) to go back to the main menu ");
                            string edit = ReceiveCmd(19);
                            int EDIT = Convert.ToInt32(edit);
                            if (EDIT == 1)
                            {
                                Console.WriteLine("Please enter the new first name ");
                                 string fName = Console.ReadLine();
                                 Console.WriteLine("Please enter the new last name ");
                                 string lName = Console.ReadLine();
                                //itsBL.updateClubMemner(id,fName,lName);
                            }
                            else
                            {
                                
                                ClubMemberQuestion(); }

                    }
                    else
                    Console.WriteLine("The Club Member is not exist in the in the system  ");
                    Console.WriteLine("Done. Would you like to edit for another club member details? Press:  ");
                    Console.WriteLine("(1) Yes ", "\n"); Console.WriteLine("(2) No, go back please ", "\n");
                    anss = ReceiveCmd(18);

                }
                ClubMemberQuestion();

            }
            if (CMQuestion == "0")
            {
                FirstQuestion();
            }
        }

        private void EmployeeQuestion()
        {
            Console.Clear();
            Console.WriteLine("Hello manager ", "\n");
            Console.WriteLine("to go back to the main menu press 0", "\n");
            Console.WriteLine("1 = Check if an Employee exist by his SSN ", "\n");
            Console.WriteLine("2 = Add a new Employee", "\n");
            Console.WriteLine("3 = Remove an Employee ", "\n");
            Console.WriteLine("4 = Get an Employees by their salaries range", "\n");
            Console.WriteLine("5 = Get an Employees by their department ID number ", "\n");
            Console.WriteLine("6 = Get a list of all the Employees ", "\n");
            Console.WriteLine("7 = Edit an Employee details ", "\n");
            Console.WriteLine("Please answer", "\n");
            EmployeeQuestionAns(ReceiveCmd(5));
        }

        private void EmployeeQuestionAns(string EmpQuestion)
        {
            if (EmpQuestion == "1")
            {
                string anss = "1";
                while (anss.Equals("1"))
                {
                    Console.Clear();
                    Console.WriteLine("please enter the employee's SSN");
                    string Id = ReceiveCmd(10);
                    int id = Convert.ToInt32(Id);
                    bool exist = itsBL.FindEmployeeByID(id);
                    if (exist)
                        Console.WriteLine("The Employee is exist in the in the system ");
                    else
                        Console.WriteLine("Sorry, the Employee is not exist in the in the system ");
                    Console.WriteLine("Done. Would you like to find another employee by hid ID? Press:  ");
                    Console.WriteLine("(1) Yes ", "\n"); Console.WriteLine("(2) No, go back please ", "\n");
                    anss = ReceiveCmd(18);

                }
                EmployeeQuestion();
            }

            if (EmpQuestion == "2")
            {
                string anss = "1";
                while (anss.Equals("1"))
                {
                    Console.Clear();
                    Console.WriteLine("please enter the Employee salary");
                    string salary = ReceiveCmd(13);
                    int Salary = Convert.ToInt32(salary);
                    Console.WriteLine("Please enter the employee 9 digit department ID");
                    string dep = ReceiveCmd(10);
                    int Dep = Convert.ToInt32(dep);
                    
                    Console.WriteLine("please enter the SSN of the supervisor of the employee");
                    string sup = ReceiveCmd(10);
                    int SUP = Convert.ToInt32(sup);
                    Console.WriteLine("please enter the employee's SSN");
                    string ssn = ReceiveCmd(10);
                    int SSN = Convert.ToInt32(ssn);
                    while (itsBL.FindEmployeeByID(SSN))
                    {
                        Console.WriteLine("the Employee's ID you entered is already exist in the system. Let's try again : ");
                        ssn = ReceiveCmd(10);
                        SSN = Convert.ToInt32(ssn);
                    }
                    Console.WriteLine("please enter the employee first name");
                    string Fname = ReceiveCmd(11);
                    Console.WriteLine("please enter the employee last name");
                    string Lname = ReceiveCmd(11);
                    Console.WriteLine("please enter the employee gender");
                    string gender = ReceiveCmd(12);
                    itsBL.AddNewEmployee(Salary, Dep, SUP, SSN, Fname, Lname, gender);
                    Console.WriteLine("The Employee added succussfuly");
                    Console.WriteLine("Done. Would you like to add another employee? Press:  ");
                    Console.WriteLine("(1) Yes ", "\n"); Console.WriteLine("(2) No, go back please ", "\n");
                    anss = ReceiveCmd(18);

                }
                EmployeeQuestion();
            }

            if (EmpQuestion == "3")
            {
                string anss = "1";
                while (anss.Equals("1"))
                {
                    Console.Clear();
                    Console.WriteLine("please enter the employee SSN");
                    string ssn = ReceiveCmd(10);
                    int SSN = Convert.ToInt32(ssn);
                    bool exist = itsBL.FindEmployeeByID(SSN);
                    while (!exist)
                    {
                        Console.WriteLine("Sorry, the Employee is not exist in the system, try again: ");
                        Console.WriteLine("please enter the employee SSN");
                        ssn = ReceiveCmd(10);
                        SSN = Convert.ToInt32(ssn);
                        exist = itsBL.FindEmployeeByID(SSN);
                    }
                    itsBL.deleteEmployee(SSN);
                    Console.WriteLine("The Employee removed succussfuly");
                    Console.WriteLine("Done. Would you like to remove another employee? Press:  ");
                    Console.WriteLine("(1) Yes ", "\n"); Console.WriteLine("(2) No, go back please ", "\n");
                    anss = ReceiveCmd(18);

                }
                EmployeeQuestion();

            }

            if (EmpQuestion == "4")
            {
                string anss = "1";
                while (anss.Equals("1"))
                {
                    Console.Clear();
                    Console.WriteLine("Please enter the minimum salary ");
                    string min = ReceiveCmd(13);
                    double MIN = Convert.ToDouble(min);
                    Console.WriteLine("Please enter the maximum salary ");
                    string max = ReceiveCmd(13);
                    double MAX = Convert.ToDouble(max);
                    while (MIN > MAX)
                    {
                        Console.WriteLine("Sorry. Incorrect input. Let's try again: ");
                        Console.WriteLine("Please enter the minimum salary ");
                        min = ReceiveCmd(13);
                        MIN = Convert.ToDouble(min);
                        Console.WriteLine("Please enter the maximum salary ");
                        max = ReceiveCmd(13);
                        MAX = Convert.ToDouble(max);
                    }
                    DisplayResult(itsBL.GetAllEmployeesBySalaryRange(MIN, MAX));
                    Console.WriteLine("Done. Would you like to find another employee by salary range? Press:  ");
                    Console.WriteLine("(1) Yes ", "\n"); Console.WriteLine("(2) No, go back please ", "\n");
                    anss = ReceiveCmd(18);

                }
                EmployeeQuestion();
            }
            if (EmpQuestion == "5")
            {
                string anss = "1";
                while (anss.Equals("1"))
                {
                    Console.Clear();
                    Console.WriteLine("please enter the employee 9 digit department ID number");
                    string id = ReceiveCmd(10);
                    int ID = Convert.ToInt32(id);
                    DisplayResult(itsBL.GetAllEmployeeListByDepID(ID));
                    Console.WriteLine("Done. Would you like to find another employee by hid department ID? Press:  ");
                    Console.WriteLine("(1) Yes ", "\n"); Console.WriteLine("(2) No, go back please ", "\n");
                    anss = ReceiveCmd(18);

                }
                EmployeeQuestion();
            }
            if (EmpQuestion == "6")
            {
                Console.Clear();
                DisplayResult(itsBL.GetAllEmployees());
                Console.ReadLine();

                EmployeeQuestion();
            }
            if (EmpQuestion == "7")
            {
                string anss = "1";
                while (anss.Equals("1"))
                {
                    Console.Clear();
                    Console.WriteLine("please enter the employee's Teudat Zeut (9 digit)");
                    string Id = ReceiveCmd(10);
                    int id = Convert.ToInt32(Id);
                    bool exist = itsBL.FindEmployeeByID(id);
                    if (exist)
                    {
                        Console.WriteLine("Would you like to update the employee First name/Last name/Teudat Zeut? ");
                        Console.WriteLine("Press (1) for yes (2) to go back to the main menu ");
                        string edit = ReceiveCmd(19);
                        int EDIT = Convert.ToInt32(edit);
                        if (EDIT == 1)
                        {
                            Console.WriteLine("Please enter the new first name ");
                            string fName = Console.ReadLine();
                            Console.WriteLine("Please enter the new last name ");
                            string lName = Console.ReadLine();
                            Console.WriteLine("Please enter the new Teudat Zeut (9 digit)");
                            string tz = ReceiveCmd(10);
                            int Newtz = Convert.ToInt32(tz);

                            //itsBL.updateEmployee(id, fName, lName,Newtz);
                        }
                        else
                        {

                            EmployeeQuestion();
                        }

                    }
                    else
                        Console.WriteLine("The Employee is not exist in the in the system  ");
                    Console.WriteLine("Done. Would you like to edit for another employee details? Press:  ");
                    Console.WriteLine("(1) Yes ", "\n"); Console.WriteLine("(2) No, go back please ", "\n");
                    anss = ReceiveCmd(18);

                }
                EmployeeQuestion();

            }

            if (EmpQuestion == "0")
            {
                FirstQuestion();
            }
        }
        private void DepartmentQuestion()
        {
            Console.Clear();
            Console.WriteLine("Hello manager ", "\n");
            Console.WriteLine("to go back to the main menu press 0", "\n");
            Console.WriteLine("1 = Check if a department exist by it ID ", "\n");
            Console.WriteLine("2 = Add a new Deparment", "\n");
            Console.WriteLine("3 = Remove Department ", "\n");
            Console.WriteLine("4 = Get a department by it's name", "\n");
            Console.WriteLine("5 = Get a list of all the Departments ", "\n");
            Console.WriteLine("6 = Edit Department Name ", "\n");
            Console.WriteLine("Please answer", "\n");
            DepartmentQuestionAns(ReceiveCmd(6));
        }

        private void DepartmentQuestionAns(string DepQuestion)
        {
            if (DepQuestion == "1")
            {
                string anss = "1";
                while (anss.Equals("1"))
                {
                    Console.Clear();
                    Console.WriteLine("please enter the 9 digit department ID number");
                    string Id = ReceiveCmd(10);
                    int id = Convert.ToInt32(Id);
                    bool exist = itsBL.FindDepartmentByID(id);
                    if (exist)
                        Console.WriteLine("The department is exist in the in the system ");
                    else
                        Console.WriteLine("Sorry, the department is not exist in the in the system ");
                    Console.WriteLine("Done. Would you like to find another department by it's ID? Press:  ");
                    Console.WriteLine("(1) Yes ", "\n"); Console.WriteLine("(2) No, go back please ", "\n");
                    anss = ReceiveCmd(18);

                }
                DepartmentQuestion();
            }
            if (DepQuestion == "2")
            {
                string anss = "1";
                while (anss.Equals("1"))
                {
                    Console.Clear();
                    Console.WriteLine("please enter the new 9 digit department ID");
                    string Id = ReceiveCmd(10);
                    int id = Convert.ToInt32(Id);
                    while (itsBL.FindDepartmentByID(id))
                    {
                        Console.WriteLine("the Department's ID you entered is already exist in the system. Let's try again : ");
                        Id = ReceiveCmd(10);
                        id = Convert.ToInt32(Id);
                    }
                    Console.WriteLine("please enter the new department name");
                    string name = ReceiveCmd(11);
                    itsBL.AddNewDepartment(id, name);
                    Console.WriteLine("The department added succufully");
                    Console.WriteLine("Done. Would you like to add another department by it's ID? Press:  ");
                    Console.WriteLine("(1) Yes ", "\n"); Console.WriteLine("(2) No, go back please ", "\n");
                    anss = ReceiveCmd(18);

                }
                DepartmentQuestion();
            }
            if (DepQuestion == "3")
            {
                string anss = "1";
                while (anss.Equals("1"))
                {
                    Console.Clear();
                    Console.WriteLine("please enter the 9 digit department ID");
                    string Id = ReceiveCmd(10);
                    int id = Convert.ToInt32(Id);
                    while (!itsBL.FindDepartmentByID(id))
                    {
                        Console.WriteLine("Sorry, the department is not exist in the in the system ");
                        Console.WriteLine("please enter the 9 digit department ID");
                        Id = ReceiveCmd(10);
                        id = Convert.ToInt32(Id);

                    }
                   itsBL.deleteDepartment(id);
                   Console.WriteLine("The department removed succufully");
                   Console.WriteLine("Done. Would you like to remove another department by it's ID? Press:  ");
                   Console.WriteLine("(1) Yes ", "\n"); Console.WriteLine("(2) No, go back please ", "\n");
                   anss = ReceiveCmd(18);
                    
                    


                }
                DepartmentQuestion();
            }
            if (DepQuestion == "4")
            {
                string anss = "1";
                while (anss.Equals("1"))
                {
                    Console.Clear();
                    Console.WriteLine("please enter the department name");
                    string name = ReceiveCmd(11);
                    DisplayResult(itsBL.GetDepartmentByName(name));
                    Console.WriteLine("Done. Would you like to get another department by it's name? Press:  ");
                    Console.WriteLine("(1) Yes ", "\n"); Console.WriteLine("(2) No, go back please ", "\n");
                    anss = ReceiveCmd(18);

                }
                DepartmentQuestion();
            }
            if (DepQuestion == "5")
            {
                Console.Clear();
                DisplayResult(itsBL.GetAllDepartments());
                Console.ReadLine();

                DepartmentQuestion();
            }
            if (DepQuestion == "6")
            {
                string anss = "1";
                while (anss.Equals("1"))
                {
                    Console.Clear();
                    Console.WriteLine("please enter the 9 digit department ID ");
                    string Id = ReceiveCmd(10);
                    int id = Convert.ToInt32(Id);
                    bool exist = itsBL.FindDepartmentByID(id);
                    if (exist)
                    {
                        Console.WriteLine("Would you like to update the department? ");
                        Console.WriteLine("Press (1) for yes (2) to go back to the main menu ");
                        string edit = ReceiveCmd(19);
                        int EDIT = Convert.ToInt32(edit);
                        if (EDIT == 1)
                        {
                            Console.WriteLine("Please enter the new  name ");
                            string Name = Console.ReadLine();
                            //itsBL.updateDepartment(id, Name);
                        }
                        else
                        {

                            DepartmentQuestion();
                        }

                    }
                    else
                        Console.WriteLine("The Department is not exist in the in the system  ");
                    Console.WriteLine("Done. Would you like to edit for another Department details? Press:  ");
                    Console.WriteLine("(1) Yes ", "\n"); Console.WriteLine("(2) No, go back please ", "\n");
                    anss = ReceiveCmd(18);

                }
                DepartmentQuestion();

            }
            if (DepQuestion == "0")
            {
                FirstQuestion();
            }


        }

        private void UserQuestion()
        {
            Console.Clear();
            Console.WriteLine("Hello manager ", "\n");
            Console.WriteLine("to go back to the main menu press 0", "\n");
            Console.WriteLine("1 = Edit exiting user ", "\n");
            Console.WriteLine("2 = Add a new user", "\n");
            Console.WriteLine("3 = Remove user ", "\n");
            Console.WriteLine("0 = Go back to the main menu ", "\n");
            Console.WriteLine("Please answer", "\n");
            UserQuestionAns(ReceiveCmd(20));
        }



        private void UserQuestionAns(string UsQuestion)
        {

            if (UsQuestion == "1")
            {
                string anss = "1";
                while (anss.Equals("1"))
                {
                    Console.Clear();
                    Console.WriteLine("please enter your user name");
                    string userName = Console.ReadLine();
                    Console.WriteLine("Enter you password");
                    int pass;
                    while (!int.TryParse(ReadPassword(), out pass))
                    {

                        Console.WriteLine("Invalid password. Try again (only numbers)");
                    }


                    while (!(itsBL.FindUser(userName, pass)))
                    {
                        Console.WriteLine("Invalid user name / password . Let's try again: ");
                        Console.WriteLine("please enter your userName");
                        userName = Console.ReadLine();
                        Console.WriteLine("Enter you password");
                        while (!int.TryParse(ReadPassword(), out pass))
                        {

                            Console.WriteLine("Invalid password. Try again (only numbers)");
                        }
                    }

                    Console.WriteLine("Please enter the new password (only digits)");
                    string newPass = Console.ReadLine();
                    while (!check(newPass))
                    {
                        Console.WriteLine("The new password you entered does not contain ONLY digit. Let's try again : ");
                        newPass = Console.ReadLine();
                    }
                    int NEWPASS = Convert.ToInt32(newPass);
                    itsBL.updateUser(userName, NEWPASS);
                    Console.WriteLine("The password update Successfully");

                    Console.WriteLine("Done. Would you like to find another department by it's ID? Press:  ");
                    Console.WriteLine("(1) Yes ", "\n"); Console.WriteLine("(2) No, go back please ", "\n");
                    anss = ReceiveCmd(18);

                }
                UserQuestion();
            }

            if (UsQuestion == "1")
            {
                string anss = "1";
                while (anss.Equals("1"))
                {
                    Console.Clear();
                    Console.WriteLine("please enter your user name");
                    string userName = Console.ReadLine();
                    Console.WriteLine("Enter you password");
                    int pass;
                    while (!int.TryParse(ReadPassword(), out pass))
                    {

                        Console.WriteLine("Invalid password. Try again (only numbers)");
                    }


                    while (!(itsBL.FindUser(userName, pass)))
                    {
                        Console.WriteLine("Invalid userName / password . Let's try again: ");
                        Console.WriteLine("please enter your userName");
                        userName = Console.ReadLine();
                        Console.WriteLine("Enter you password");
                        while (!int.TryParse(ReadPassword(), out pass))
                        {

                            Console.WriteLine("Invalid password. Try again (only numbers)");
                        }
                    }

                    Console.WriteLine("Please enter the new password (only digits)");
                    string newPass = Console.ReadLine();
                    while (!check(newPass))
                    {
                        Console.WriteLine("The new password you entered does not contain ONLY digit. Let's try again : ");
                        newPass = Console.ReadLine();
                    }
                    int NEWPASS = Convert.ToInt32(newPass);
                    itsBL.updateUser(userName, NEWPASS);
                    Console.WriteLine("The password update Successfully");

                    Console.WriteLine("Done. Would you like to update another user password? Press:  ");
                    Console.WriteLine("(1) Yes ", "\n"); Console.WriteLine("(2) No, go back please ", "\n");
                    anss = ReceiveCmd(18);

                }
                UserQuestion();
            }


            if (UsQuestion == "2")
            {
                string anss = "1";
                while (anss.Equals("1"))
                {
                    Console.Clear();
                    Console.WriteLine("please enter your user name");
                    string userName = Console.ReadLine();
                    Console.WriteLine("Enter you password");
                    int pass;
                    while (!int.TryParse(ReadPassword(), out pass))
                    {

                        Console.WriteLine("Invalid password. Try again (only numbers)");
                    }


                    while (!(itsBL.FindUser(userName, pass)))
                    {
                        Console.WriteLine("Invalid userName / password . Let's try again: ");
                        Console.WriteLine("please enter your userName");
                        userName = Console.ReadLine();
                        Console.WriteLine("Enter you password");
                        while (!int.TryParse(ReadPassword(), out pass))
                        {

                            Console.WriteLine("Invalid password. Try again (only numbers)");
                        }
                    }
                    Console.WriteLine("Please enter the new userName ");
                    string newUser = Console.ReadLine();
                    Console.WriteLine("Please enter the new password (only digits)");
                    string newPass = Console.ReadLine();
                    while (!check(newPass))
                    {
                        Console.WriteLine("The new password you entered does not contain ONLY digit. Let's try again : ");
                        newPass = Console.ReadLine();
                    }
                    int NEWPASS = Convert.ToInt32(newPass);
                    User U = new User(newUser, NEWPASS);
                    itsBL.addUser(U);
                    Console.WriteLine("The new user added Successfully");

                    Console.WriteLine("Done. Would you like to add another user? Press:  ");
                    Console.WriteLine("(1) Yes ", "\n"); Console.WriteLine("(2) No, go back please ", "\n");
                    anss = ReceiveCmd(18);

                }
                UserQuestion();
            }

            if (UsQuestion == "3")
            {
                string anss = "1";
                while (anss.Equals("1"))
                {
                    Console.Clear();
                    Console.WriteLine("please enter your userName");
                    string userName = Console.ReadLine();
                    Console.WriteLine("Enter you password");
                    int pass;
                    while (!int.TryParse(ReadPassword(), out pass))
                    {

                        Console.WriteLine("Invalid password. Try again (only numbers)");
                    }


                    while (!(itsBL.FindUser(userName, pass)))
                    {
                        Console.WriteLine("Invalid userName / password . Let's try again: ");
                        Console.WriteLine("please enter your user name");
                        userName = Console.ReadLine();
                        Console.WriteLine("Enter you password");
                        while (!int.TryParse(ReadPassword(), out pass))
                        {

                            Console.WriteLine("Invalid password. Try again (only numbers)");
                        }
                    }
                    Console.WriteLine("Please enter the new userName ");
                    string newUser = Console.ReadLine();
                    Console.WriteLine("Please enter the new password (only digits)");
                    string newPass = Console.ReadLine();
                    while (!check(newPass))
                    {
                        Console.WriteLine("The new password you entered does not contain ONLY digit. Let's try again : ");
                        newPass = Console.ReadLine();
                    }
                    int NEWPASS = Convert.ToInt32(newPass);
                    User U = new User(newUser, NEWPASS);
                    itsBL.removeUser(U);
                    Console.WriteLine("The new user have been romved Successfully");

                    Console.WriteLine("Done. Would you like to remove another user? Press:  ");
                    Console.WriteLine("(1) Yes ", "\n"); Console.WriteLine("(2) No, go back please ", "\n");
                    anss = ReceiveCmd(18);

                }
                UserQuestion();
            }

            if (UsQuestion == "0")
            {
                FirstQuestion();
            }



        }

    }
}

