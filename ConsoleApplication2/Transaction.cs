﻿using System;
using System.Collections.Generic;
using System.Linq;  
using System.Text;
using System.Threading.Tasks;


namespace BL_backend
{
    [Serializable]
    public enum PaymentMethod
    {
        Cash,
        Visa,
        MasterCard,
        AmericanExpress,
        Isracard,
        Check,
        Other
    }
    [Serializable]
    public class Transaction
    {
        private PaymentMethod paymentMethod;
        private string transactionID;
        private DateTime dateTime;
        private bool isAReturn;
        private Dictionary<int, SoldProducts> receipt = new Dictionary<int, SoldProducts>();



        public Transaction(DateTime dateTime, Boolean isAReturn, PaymentMethod paymentMethod, Dictionary<int, SoldProducts> receipt)
        {
            this.transactionID = System.Guid.NewGuid().ToString();
            this.dateTime = dateTime;
            this.isAReturn = isAReturn;
            this.paymentMethod = paymentMethod;
            this.receipt = receipt;

        }

        public Transaction()
        {
            transactionID = null;
            dateTime = new DateTime(2015, 1, 1);
            isAReturn = false;
            paymentMethod = PaymentMethod.Other;
        }


        public string TransactionID
        {
            get { return transactionID; }
            set { transactionID = value; }
        }


        public DateTime DateTime
        {
            get { return dateTime; }
            set { dateTime = value; }
        }


        public bool IsAReturn
        {
            get { return isAReturn; }
            set { isAReturn = value; }
        }


        public Dictionary<int, SoldProducts> Receipt
        {
            get { return receipt; }
            set { receipt = value; }
        }


        public PaymentMethod PaymentMethod
        {
            get { return paymentMethod; }
            set { paymentMethod = value; }
        }

        //public string getID()
        //{
        //    return transactionID;
        //}

        public DateTime getDateTime()
        {
            return dateTime;
        }

        //public void setDateTime(DateTime dateTime)
        //{
        //    this.dateTime = dateTime;
        //}

        //public bool getIsAReturn()
        //{
        //    return isAReturn;
        //}

        //public void setIsAReturn(bool isAReturn)
        //{
        //    this.isAReturn = isAReturn;
        //}

        //public PaymentMethod getPaymentMethod()
        //{
        //    return paymentMethod;
        //}

        //public void setPaymentMethod(PaymentMethod paymentMethod)
        //{
        //    this.paymentMethod = paymentMethod;
        //}

        [Serializable]
        public class SoldProducts
        {
            private int ID;
            private int quantity;
            private double price;

            public int ID1
            {
                get { return ID; }
                set { ID = value; }
            }


            public double Price
            {
                get { return price; }
                set { price = value; }
            }


            public int Quantity
            {
                get { return quantity; }
                set { quantity = value; }
            }


            public SoldProducts(Product product, int quantity)
            {
                ID = product.InventoryID1;
                Price = product.getPrice();
                this.quantity = quantity;
            }



            //public int getIDD()
            //{
            //    return ID;
            //}

            //public void setIDD(int transactionID)
            //{
            //    this.ID = transactionID;
            //}
            //public double Price { get; set; }

            //public int getQua()
            //{
            //    return quantity;
            //}

            //public void setQua(int quantity)
            //{
            //    this.quantity = quantity;
            //}

            public string toString()
            {
                return "The product you purchase are : " + "\n" + "ID number : " + ID1.ToString() + "\n" + "price : " + Price.ToString() + "\n" + " Quantity : " + Quantity.ToString() + "\n";
            }
        }

        public override string ToString()
        {
            string result;
            switch (PaymentMethod)
            {
                case PaymentMethod.AmericanExpress:
                    result = "Transaction date : " + DateTime.ToString() + "\n" + "Is the transaction return - " + IsAReturn + "\n" + "Payment mathod : " + PaymentMethod.AmericanExpress.ToString() + "thedjdj";
                    break;
                case PaymentMethod.Cash:
                    result = "Transaction date : " + DateTime.ToString() + "\n" + "Is the transaction return - " + IsAReturn + "\n" + "Payment mathod : " + PaymentMethod.Cash.ToString();
                    break;
                case PaymentMethod.Check:
                    result = "Transaction date : " + DateTime.ToString() + "\n" + "Is the transaction return - " + IsAReturn + "\n" + "Payment mathod : " + PaymentMethod.Check.ToString();
                    break;
                case PaymentMethod.Isracard:
                    result = "Transaction date : " + DateTime.ToString() + "\n" + "Is the transaction return - " + IsAReturn + "\n" + "Payment mathod : " + PaymentMethod.Isracard.ToString();
                    break;
                case PaymentMethod.MasterCard:
                    result = "Transaction date : " + DateTime.ToString() + "\n" + "Is the transaction return - " + IsAReturn + "\n" + "Payment mathod : " + PaymentMethod.MasterCard.ToString();
                    break;
                case PaymentMethod.Other:
                    result = "Transaction date : " + DateTime.ToString() + "\n" + "Is the transaction return - " + IsAReturn + "\n" + "Payment mathod : " + PaymentMethod.Other.ToString();
                    break;
                case PaymentMethod.Visa:
                    result = "Transaction date : " + DateTime.ToString() + "\n" + "Is the transaction return - " + IsAReturn + "\n" + "Payment mathod : " + PaymentMethod.Visa.ToString();
                    break;
                default:
                    result = "Transaction date : " + DateTime.ToString() + "\n" + "Is the transaction return - " + IsAReturn + "\n" + "Payment mathod : " + PaymentMethod.Other.ToString();
                    break;
            }
            foreach (KeyValuePair<int, SoldProducts> entry in Receipt)
            {
                result += entry.Key + " " + entry.Value.toString();
            }
            return result;
        }
    }
}
