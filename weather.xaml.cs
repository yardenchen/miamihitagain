﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BL;
using BL_backend;
using System.Xml;

namespace GUI
{
    
    public partial class weather : Window
    {
        private IBL itsBL;
        private User U;
      
        string tempature;
        string condition;
        string humidity;
        string windspeed;
        string town;
        string TFCond;
        string TFHigh;
        string TFLow;
        string town_id;
        public weather(string town_id, IBL IBL, User U)
        {
            this.itsBL = IBL;
            this.U = U;
            this.town_id = town_id;
            InitializeComponent();
        }
        private void GetWeather()
        {
            string query = string.Format("http://weather.yahooapis.com/forecastrss?w=" + town_id);
            XmlDocument wdata = new XmlDocument();
            wdata.Load(query);

            XmlNamespaceManager manager = new XmlNamespaceManager(wdata.NameTable);
            manager.AddNamespace("yweather", "http://xml.weather.yahoo.com/ns/rss/1.0");

            XmlNode channel = wdata.SelectSingleNode("rss").SelectSingleNode("channel");
            XmlNodeList nodes = wdata.SelectNodes("rss/channel/item/yweather:forcast", manager);
            tempature = (string)channel.SelectSingleNode("item").SelectSingleNode("yweather:condition", manager).Attributes["temp"].Value;
            condition = channel.SelectSingleNode("item").SelectSingleNode("yweather:condition", manager).Attributes["text"].Value;
            humidity = channel.SelectSingleNode("yweather:atmosphere", manager).Attributes["humidity"].Value;
            windspeed = channel.SelectSingleNode("yweather:wind", manager).Attributes["speed"].Value;
            town = channel.SelectSingleNode("yweather:location", manager).Attributes["city"].Value;
            TFCond = channel.SelectSingleNode("item").SelectSingleNode("yweather:forecast", manager).Attributes["text"].Value;
            TFHigh = channel.SelectSingleNode("item").SelectSingleNode("yweather:forecast", manager).Attributes["high"].Value;
            TFLow = channel.SelectSingleNode("item").SelectSingleNode("yweather:forecast", manager).Attributes["low"].Value;
        }

        private void Refresh_btn(object sender, RoutedEventArgs e)
        {
            GetWeather();
            towntxt.Text = town;
            conditiontxt.Text = condition;
            humiditytxt.Text = humidity;
            TFContxt.Text = TFCond;
            TFHightxt.Text = TFHigh;
            TFLowtxt.Text = TFLow;
            temptxt.Text = tempature;
            windtxt.Text = windspeed;
        }

        private void back_btn(object sender, RoutedEventArgs e)
        {
            Stores s = new Stores(itsBL, U);
            s.Show();
            this.Close();
        }

        private void Button_Loaded(object sender, RoutedEventArgs e)
        {
            GetWeather();
            towntxt.Text = town;
            conditiontxt.Text = condition;
            humiditytxt.Text = humidity;
            TFContxt.Text = TFCond;
            TFHightxt.Text = TFHigh;
            TFLowtxt.Text = TFLow;
            temptxt.Text = tempature;
            windtxt.Text = windspeed;
        }
    }
}